<!DOCTYPE html>
<html>


<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Aplikasi Rapor Digital</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/simple-line-icons/css/simple-line-icons.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/flag-icon-css/css/flag-icon.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/perfect-scrollbar/css/perfect-scrollbar.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/icheck/skins/all.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/select2/dist/css/select2.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/select2-bootstrap-theme/dist/select2-bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/dropify/dist/css/dropify.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/node_modules/bootstrap-tour/build/css/bootstrap-tour-standalone.min.css">
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/logo_kemenag.png">


</head>
 <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar navbar-light col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper"> <a class="navbar-brand brand-logo" href="index.html"><img src="<?php echo base_url();?>assets/images/logo_ardira.png" alt="logo"></a> <a class="navbar-brand brand-logo-mini" href="index.html"><img src="<?php echo base_url();?>assets/images/logo_ardira.png" alt="logo"></a> </div>
      <div class="navbar-menu-wrapper d-flex align-items-center">
        <button class="navbar-toggler navbar-toggler align-self-center mr-2" type="button" data-toggle="minimize"> <span class="mdi mdi-equal display-3"></span> </button>
       <!-- <form class="form-inline mt-2 mt-md-0 d-none d-lg-block">
          <div class="input-group search"> <span class="input-group-addon bg-transparent" id="basic-addon1"><i class="mdi mdi-magnify"></i></span> <input type="text" class="form-control bg-transparent" placeholder="Search..." aria-label="Username" aria-describedby="basic-addon1"> </div>
        </form>-->
        <ul class="navbar-nav nav-header-item-wrapper">
          <li class="nav-item d-none d-sm-block dropdown">
           <a class="btn bg-transparent dropdown-toggle text-default-color" id="userDropdown" href="#" data-toggle="dropdown"> 
           <?php
                $header = $sekolah;
                ?>
                <?php echo $header[0]->nama_sekolah ?>
          </a>
            <div class="dropdown-menu navbar-dropdown preview-list p-3" aria-labelledby="userDropdown"> 
              <div class="dropdown-divider mb-4"></div><a href="<?php echo site_url('login/logout');?>" class="btn btn-block btn-primary">Logout</a> </div>
          </li>         
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas"> <span class="mdi mdi-equal display-3"></span> </button>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
      <ul class="nav">
      <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('home');?>"> <i class="mdi mdi-compass-outline menu-icon"></i> <span class="menu-title">Dashboard</span> </a> </li>
        
          
       
       <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('UploadData');?>"> <i class="mdi mdi-telegram menu-icon"></i> <span class="menu-title">Kirim Data</span> </a> </li>
        <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('home/data');?>"> <i class="mdi mdi-telegram menu-icon"></i> <span class="menu-title">Data Bulanan</span> </a> </li>
      
         
           

        </ul>
      </nav></html>