<!DOCTYPE html>
<html lang="en">
<?php require_once('header.php'); 
  ?>

  
<body>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
           <div class="col-lg-12 grid-margin stretch-card">
               <div class="card">
                <div class="card-body">
                  <?php echo $this->session->flashdata('msg2');?>
                <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Data Admin Provinsi</li>
                      </ol>
                </nav>
                
                  <div class="row">
                  <div class="col-lg-6">
                    <h4 class="card-title">Admin Provinsi</h4>
                  </div> 
                  <div class="col-lg-6 text-right">
                    <a  class="btn btn-light btn-sm"  href="<?php echo site_url('pusat/admintambah')?>">Tambah</a>
                  </div>
                <div class="col-lg-12">
                  <div class="table-responsive">
                    <br>
                  <table id="mytable" class="table table-striped table-advance table-hover">
                    <thead>
                      <tr> 
                         <th>No</th>
                         <th>Username</th>
                          <th>Provinsi</th>
                          <th>Aksi</th>
                        
                      </tr>
                    </thead>
                    
                   <tbody>
                        <?php
                        $no = 1;
                        foreach($admin->result() as $b){
                        ?>
                        <tr>
                          <td><?php echo $no++?></td>
                          <td> <?php echo $b->username?></td>
                          <td> <?php echo $b->nama_provinsi?></td>
                          <td>  <a  class='edit-record btn btn-primary btn-sm' data-id='<?php echo $b->id_user?>' style="color:white;">
                            <i class='icon-pencil'></i> Ubah</a>
                             <a class='btn btn-primary btn-sm' onClick='return konfirmasi();' href="<?php echo site_url('pusat/hapusadmin/').$b->id_user.'/'?>"> <i class='icon-trash'></i> Hapus</a></td>
                       
                      </tr>
                                <?php
                                    }
                                ?>
                  </tbody>
                  </table>
                </div>
                </div>
                </div>
                </div>

                  
                </div>
              </div>
            </div>
        </div>
         <div class="modal fade" id="exampleModal-2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
   <form action="<?php echo site_url('pusat/aksi_edit/')?>" method="post" enctype="multipart/form-data">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel-2">Detail Akun</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          <p></p>
                        </div>
                        <div class="modal-footer">
                          <button type="Submit" class="btn btn-success">Submit</button>
                          <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
                        </div>
                      </div>
                    </div>
                  </form>
                  </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
           <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal</i></span>
             </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

 <?php $this->load->view('footer.php'); ?>
 <script type="text/javascript" language="JavaScript">
 function konfirmasi()
 {
 tanya = confirm("Anda Yakin Menghapus Data Ini ?");
 if (tanya == true) return true;
 else return false;
 }</script>
  <script>
            $(function () {
                $(document).on('click', '.edit-record', function (e) {
                    e.preventDefault();
                    $("#exampleModal-2").modal('show');
                    $.post('<?php echo site_url('pusat/edit_akun');?>',
                            {id: $(this).attr('data-id')},
                    function (html) {
                        $(".modal-body").html(html);
                    }
                    );
                });
            });
        </script>
        
</body>

</html>