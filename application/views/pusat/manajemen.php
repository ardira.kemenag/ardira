<!DOCTYPE html>
<html lang="en">
<?php require_once('header.php'); 
  ?>
<body>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
           <div class="col-lg-12 grid-margin stretch-card">
               <div class="card">
                <div class="card-body">
                   <?php echo $this->session->flashdata('msg2');?>
                  
                <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Data Logs</li>
                      </ol>
                </nav>
                
                  <div class="row">
                  <div class="col-lg-6">
                    <h4 class="card-title">Data Logs</h4>
                  </div> 
                 
                <div class="col-lg-12">
                  <div class="table-responsive">
                    <br>
                  <table id="mytable" class="table table-striped table-advance table-hover">
                    <thead>
                      <tr> 
                         <th>No</th>
                         <th> User </th>
                        <th> Date login </th>
                       <!--  <th> Aksi </th> -->
                      </tr>
                    </thead>
                    
                 <tbody>
                        <?php
                        if($this->uri->segment(3)){
                          $no=$this->uri->segment(3)+1;
                        }else{
                          $no = 1;
                        }
                        
                        foreach($logs->result() as $b){
                        ?>
                        <tr>
                          <td> <?php echo $no++ ;?>
                           <td>  <?php if($b->id_sekolah=='0' && $b->id_provinsi=='0'){
                                    echo $b->username;
                                   } elseif($b->id_sekolah!=0){
                                     echo $this->model_m->cek_sekolah($b->id_sekolah)."(".$b->username.")";
                                   }  else{
                                    echo $b->nama_role.' '.$this->model_m->cek_prov($b->id_provinsi);
                                   }
                                ?>
                           </td>
                           <td>  <?php echo $b->date_login ?> </td>
                  
                       <!--  <td> 
                          <a class='btn btn-primary btn-sm' href="<?php echo site_url('Kelas/ubah_kelas/').$b->id_kelas?>"><i class='icon-pencil'></i> Ubah</a>
                          <?php 
                             $where=  array('id_kelas' => $b->id_kelas );
                             $data = $this->model_m->selectX('siswa',$where);   
                              $row = $data->row();  
                           if(!isset($row)) { ?>
                                <a class='btn btn-primary btn-sm' onClick='return konfirmasi();' href="<?php echo site_url('kelas/hapus_kelas/').$b->id_kelas?>">
                            <i class='icon-trash'></i> hapus</a>
                          <?php }else{
                            echo "";
                                 } ?>
                           </td> -->
                            
                      </tr>
                                <?php
                                    }
                                ?>
                  </tbody>
                  </table>
                 <!--  <div><ul class="pagination">
                  <?php foreach ($links as $link) {
                    echo $link;
                  }?>
                </ul>
              </div> -->
                </div>
                </div>
                </div>
                </div>

                  
                </div>
              </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
           <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal</i></span>
             </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

 <?php $this->load->view('footer.php'); ?>
</body>
<script type="text/javascript" language="JavaScript">
 function konfirmasi()
 {
 tanya = confirm("Anda Yakin Menghapus Data Ini ?");
 if (tanya == true) return true;
 else return false;
 }</script>

</html>