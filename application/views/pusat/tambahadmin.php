<!DOCTYPE html>
<html lang="en">
<?php require_once('header.php'); 
  ?>

<body>
 
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
         
        
        <div class="row">
             <div class="col-12 stretch-card">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Tambah Admin </h4>
                      <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="<?php echo site_url('KepalaRA');?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo site_url('pusat/admin');?>">Data admin</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Tambah admin</li>
                      </ol>
                    </nav>
                      <form class="forms-sample" action="<?php echo site_url('Pusat/aksi_insert/');?>" method="post" enctype="multipart/form-data">
                        
                        <div class="form-group row">
                          <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Username </label>
                         <div class="col-sm-9">
                          <input type="hidden" name="sekolah" class="form-control form-control-lg" value="<?=$this->session->userdata('id_sekolah');?>">
                           <input type="text" name="f1" class="form-control form-control-lg" placeholder=" Username">
                           
                           <input type="hidden" name="role" class="form-control form-control-lg" value="4">
                           <input type="hidden" name="status" class="form-control form-control-lg" value="1">
                           </div >
                           
                      </div>
                      <div class="form-group row">
                          <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Password </label>
                         <div class="col-sm-9">
                          
                           <input type="password" name="f2" class="form-control form-control-lg" placeholder=" password">
                          
                           </div >
                           
                      </div>
                        <div class="form-group row">
                          <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Provinsi </label>
                         <div class="col-sm-9">
                          
                          <select class="form-control form-control-md" name="prov">
                            <option>--pilih provinsi--</option>
                               <?php
                            foreach($prov->result() as $r){
                            echo "<option value='".$r->id_provinsi."'>".$r->nama_provinsi."</option>";}?>
                            
                          </select>
                          
                           </div >
                           
                      </div>

                       

                        <button type="submit" class="btn btn-success mr-2">Submit</button>
                        <a href="<?php echo site_url('Siswa/siswa');?>" class="btn btn-danger" value="Batal"><i class="icon-remove"></i> Batal</a>
                      </form>
                    </div>
                  </div>
                </div>
           
            
          </div>
         
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 . All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal</span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

 <?php $this->load->view('footer.php'); ?>
</body>

</html>