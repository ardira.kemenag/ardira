<?php
    $objPHPExcel = new PHPExcel();
      // Set document properties
      $objPHPExcel->getProperties()->setCreator("PS")
                     ->setLastModifiedBy("PS")
                     ->setTitle("Office 2007 XLSX Test Document")
                     ->setSubject("Office 2007 XLSX Test Document")
                     ->setDescription("masalah")
                     ->setKeywords("office 2007 openxml php")
                     ->setCategory("Test result file");


      // Add some data
      $styleArray = array(
          'font'  => array(
          'bold'  => true,
          'color' => array('rgb' => 'FF0000'),
          'size'  => 15,
      ));

      $objPHPExcel->setActiveSheetIndex(0);
      $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:E1');
      $objPHPExcel->setActiveSheetIndex(0)->getStyle('A1:E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('W1:AA1');
      $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('A')->setAutosize(true);
      $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('B')->setAutosize(true);
      $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('C')->setAutosize(true);
      $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('D')->setAutosize(true);
      $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('E')->setAutosize(true);
      
      $objPHPExcel->setActiveSheetIndex(0)->getStyle('A2:E2')->getFont()->setBold(true);
      $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'DATA RAUDHATUL ATHFAL');
      $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A2', 'NSM');
      $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B2', 'NAMA RA');
      $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C2', 'KEPALA RA');
      $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D2', 'PROVINSI ');
      $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E2', 'KABUPATEN');
      $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F2', 'KELURAHAN ');
      $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G2', 'KECAMATAN ');
      $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H2', 'ALAMAT ');
      $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I2', 'KODE POS ');
      $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J2', 'USERNAME ');
      $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K2', 'PASSWORD ');
      for ($i=1;$i<=count($skl);$i++){
        $object = $skl[$i-1];
       $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.($i+2), $object->NSN);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.($i+2), $object->nama_sekolah);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.($i+2), $object->kepala_RA);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.($i+2), $object->nama_provinsi);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.($i+2), $object->nama_kota);
       $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.($i+2), $object->kelurahan);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.($i+2), $object->kecamatan);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.($i+2), $object->jalan);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.($i+2), $object->kode_pos);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.($i+2), $object->username);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.($i+2), $object->password);
        
      }
      // Rename worksheet
      $objPHPExcel->getActiveSheet()->setTitle('DATA RA');


      // Set active sheet index to the first sheet, so Excel opens this as the first sheet
      $objPHPExcel->setActiveSheetIndex(0);


      // Redirect output to a client’s web browser (Excel2007)
      header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
      header('Content-Disposition: attachment;filename="Data_RA'.date('d-m-Y H:i:s').'.xlsx"');
      header('Cache-Control: max-age=0');
      // If you're serving to IE 9, then the following may be needed
      header('Cache-Control: max-age=1');

      // If you're serving to IE over SSL, then the following may be needed
      header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
      header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
      header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
      header ('Pragma: public'); // HTTP/1.0

      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
      $objWriter->save('php://output');
      exit;

?>