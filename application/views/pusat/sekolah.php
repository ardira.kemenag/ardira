<!DOCTYPE html>
<html lang="en">
<?php require_once('header.php'); 
  ?>

  
<body>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
           <div class="col-lg-12 grid-margin stretch-card">
               <div class="card">
                <div class="card-body">
                  <?php echo $this->session->flashdata('msg2');?>
                <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="<?php echo site_url('Pusat')?>">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Data RA</li>
                      </ol>
                </nav>
                <?php 
                if($this->uri->segment(2)=='sekolah_search'){$p= $this->input->post('provinsi');$kota= $this->input->post('kabupaten');
                $link='Sekolah/cetak/'.$p.'/'.$kota;
              }
                else{
                  $link='Sekolah/cetak/';
                }
                  ?>
                  <div class="row">
                  <div class="col-lg-6">
                    <h4 class="card-title">Data RA</h4>
                  </div> 
                  <div class="col-lg-6 text-right">
                    <a  class="btn btn-light btn-sm"  href="<?php echo site_url('Sekolah')?>">Tambah</a>
                  </div>
                <div class="col-lg-12">
                       <form method="post" action="<?php echo site_url('Sekolah/sekolah_search');?>">
                     <div class="form-group row"> 
                          <label  class="col-sm-2 col-form-label" >Pilih Provinsi </label>
                         <div class="col-sm-2">
                           <select required name="provinsi" class=" form-control form-control-sm" id="prov"  onchange="get_kota()" style="margin-left: -60px;">
                          
                           <option value=""> Pilih Provinsi </option>
                            <?php
                            foreach($prov as $r){?>
                              <option value="<?php echo $r->id_provinsi; ?>" <?php if($p==$r->id_provinsi)echo "selected"; ?>>  <?php echo $r->nama_provinsi; ?></option>
                           <?php }
                        ?> 
                         </select>
                           </div> 
                            <label for="exampleInputPassword2" class="col-form-label" style="
                                margin-left: -30px;">Pilih Kabupaten </label>
                            <div class="form-group row col-lg-6" >
                           <div class="col-sm-5">
                            <select required name="kabupaten" class="form-control form-control-sm" id="div_kota">

                           <option value=""> --Pilih Kota/kab-- </option>
                            <?php
                            foreach($kab as $k){?>
                              <option value="<?php echo $k->id_kotaKab; ?>" <?php if($kota==$k->id_kotaKab)echo "selected"; ?>>  <?php echo $k->nama_kota; ?></option>
                           <?php }
                        ?> 
                            
                          </select>
                        </div>
                        <span class="input-group-btn"  >
                          
                          <a href="<?php echo site_url($link);?>" class="btn btn-success btn-sm" >Cetak Excel
                                </a>
                                <button class="btn btn-primary btn-sm" type="submit">Cari
                                </button>
                            </span>
                         </div>
                         
                      </div>

                         
                        </form>
                  <div class="table-responsive">
                    <br>
                  <table id="mytable" class="table table-striped table-advance table-hover">
                    <thead>
                      <tr> 
                         <th>No</th>
                         <th><center>Nama Sekolah</center></th>
                        <th><center>NSM</center></th>
                        <th><center>Provinsi</center></th>
                        <th><center>Kota/Kab</center></th>
                        <th><center>Kepala Sekolah</center></th>
                        <th><center>Aksi</center></th>
                      </tr>
                    </thead>
                    
                   <tbody>
                        <?php
                        $no = 1;
                        foreach($skl->result() as $b){
                        ?>
                        <tr>
                          <td><center> <?php echo $no++?></center></td>
                          <td><center> <?php echo $b->nama_sekolah?></center></td>
                          <td><center> <?php echo $b->NSN ?></center></td>
                          <td><center> <?php echo $b->nama_provinsi ?></center></td>
                          <td><center> <?php echo $b->nama_kota ?></center></td>
                          <td><center> <?php echo $b->kepala_RA ?></center></td>
                           <td><center>
                         
                             <a class='btn btn-primary btn-sm' target="_blank" href="<?php echo site_url('Sekolah/data/').$b->id_sekolah?>"> <i class='icon-eye'></i> Detail</a>
                              <a class='btn btn-primary btn-sm-xs' onClick='return konfirmasi();' href="<?php echo site_url('pusat/banned/').$b->id_sekolah?>">
                            <i class='icon-trash'></i> hapus</a>
                            </td>

                          </center></td>
                       
                      </tr>
                                <?php
                                    }
                                ?>
                  </tbody>
                  </table>
                </div>
                </div>
                </div>
                </div>

                  
                </div>
              </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
           <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal</i></span>
             </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

 <?php $this->load->view('footer.php'); ?>
 <script type="text/javascript" language="JavaScript">
 function konfirmasi()
 {
 tanya = confirm("Anda Yakin Menghapus Data Ini ?");
 if (tanya == true) return true;
 else return false;
 }</script>
  <script>
            function get_kota(){
                var id_provinsi = $("#prov").val();
                $.ajax({ 
                    type: 'POST', 
                    url: "<?php echo site_url('sekolah/get_kota2'); ?>", 
                    data:"id_provinsi="+id_provinsi, 
                    success: function(msg) {
                            $("#div_kota").html(msg);
                    }
                });
            }
        </script>
</body>

</html>