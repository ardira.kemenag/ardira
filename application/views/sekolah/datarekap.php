<!DOCTYPE html>
<html lang="en">
<?php require_once('header.php'); 
  ?>
<body>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            
           <div class="col-lg-12 grid-margin stretch-card">
               <div class="card">
                <div class="card-body">
                <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="<?php echo site_url('home/dashboard')?>">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Data Upload Rekap Semester</li>
                      </ol>
                </nav>
                
                  <div class="row">
                  <div class="col-lg-6">
                    <h4 class="card-title">Data Upload Rekap Semester</h4>
                  </div> 
                  <div class="col-lg-6 text-right">
                   <!--  <a  class="btn btn-light btn-sm"  href="<?php echo site_url('Siswa/siswatmbh')?>">Tambah</a> -->

                  </div>
                  <div class="col-lg-6 text-left">
                   
                  </div>
                <div class="col-lg-12">
                  <div class="table-responsive">
                    <br>
                  <table id="mytable" class="table table-striped table-advance table-hover">
                    <thead>
                      <tr> 
                         <th>No</th>
                        
                         <th><center>Tahun Ajaran</center></th>
                          <th><center>Semester</center></th>
                         <th><center>Status</center></th>
                         <th><center>Aksi</center></th>
                      </tr>
                    </thead>
                    
                 <tbody>
                        <?php
                        $no = 1;
                        foreach($data->result() as $b){
                        ?>
                        <tr>
                          <td><center> <?php echo $no++ ?></center></td>
                       
                         <td><center> <?php echo $b->nama_tahun ?></center></td>
                         <td><center> <?php echo $b->semester ?></center></td>
                         <td><center>
                            <label class="badge badge-success"><i class='icon-check'></i>Terkirim</label>
                          </center></td>
                          <td> <a class='btn btn-primary btn-sm' target="_blank" href="<?php echo site_url('Rekap/rkp/').$b->semester.'/'.$b->id_sekolah.'/'.$b->id_tahunajaran?>"> <i class='mdi mdi-printer'></i> Cetak</a>
                            <a class='btn btn-danger btn-sm' onClick='return konfirmasi();' href="<?php echo site_url('Rekap/deleterekapsklh/').$b->id_sekolah.'/'.$b->id_tahunajaran.'/'.$b->semester?>"> <i class='icon-trash'></i> Delete</a></td>
                            
                      </tr>
                                <?php
                                    }
                                ?>
                  </tbody>
                  </table>
                </div>
                </div>
                </div>
                </div>

                  
                </div>
              </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
           <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal</i></span>
             </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

 <?php $this->load->view('footer.php'); ?>
</body>
     <script type="text/javascript" language="JavaScript">
 function konfirmasi()
 {
 tanya = confirm("Anda Yakin Menghapus Data Ini ?");
 if (tanya == true) return true;
 else return false;
 }</script>
</html>