<!DOCTYPE html>
<html lang="en">
<?php require_once('header.php'); 
  ?>

<body>
 
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-lg-12 grid-margin" style=" background: #f3829b82;">
              <div class="card chart-bg">
                <div class="card-body" style="background: #f7bdca69;">
                  <div class="wrapper d-flex justify-content-center mt-4 mb-4">
                    <div class="icon" style="padding-top: 10px;padding-right: 10px;"><i class="icon-home icon-md "></i></div>
                    <div class="details">
                       <h1 class="mb-1 mt-2">Selamat Datang Di Aplikasi Rapor Digital RA</h1>
                      <h3> Raudhatul Athfal <?php
                        $header = $sekolah;
                        ?>
                        <?php echo $header[0]->nama_sekolah ?> </h3>
                        <h4>Untuk yang baru pertama kali Login / belum sama sekali sinkron, Silakan Download pada tombol "DOWNLOAD APLIKASI"</h4>
                        <br>
                        <h4>Kemudian langsung update aplikasi</h4>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>
             <div class="col-lg-12 grid-margin">
            <div class="alert alert-fill-warning" role="alert">
                    <i class="mdi mdi-alert-circle"></i>
                    Update terbaru aplikasi! Silakan klik "update aplikasi" pada aplikasi offline
                  </div>
                </div>
          </div>
           <a  class="btn btn-success btn-lg" target="_blank" href="<?php echo site_url('KepalaRA/recent_download')?>" style="margin-bottom: 10px; " data-toggle="tooltip" data-placement="bottom" title="Silakan Download versi terbaru"><i class="mdi mdi-cloud-download"></i>Download Aplikasi Rapor Digital RA</a>
          
           <!-- <a  class="btn btn-success btn-lg" target="_blank" href="<?php echo base_url('assets/ardira.zip')?>" style="margin-bottom: 10px; "><i class="mdi mdi-cloud-download"></i>Download Aplikasi Rapor Digital RA</a> -->
          
		   <h4 class="card-title">Sikronisasi</h4>
               <div class="timeline">
                      <div class="timeline-wrapper timeline-wrapper-warning">
                        <div class="timeline-badge"></div>
                        <div class="timeline-panel">
                          <div class="timeline-heading">
                            <h6 class="timeline-title">Download Aplikasi</h6>
                          </div>
                         <!--  <div class="timeline-body">
                            <p>'Klik "'</p>
                          </div>
                          <div class="timeline-footer d-flex align-items-center">
                              <i class="mdi mdi-heart-outline text-muted mr-1"></i>
                              <span>19</span>
                              <span class="ml-auto font-weight-bold">19 Oct 2017</span>
                          </div> -->
                        </div>
                      </div>
                      <div class="timeline-wrapper timeline-inverted timeline-wrapper-danger">
                        <div class="timeline-badge"></div>
                        <div class="timeline-panel">
                          <div class="timeline-heading">
                            <h6 class="timeline-title"> Instal Aplikasi</h6>
                          </div>
                          
                        </div>
                      </div>
                      <div class="timeline-wrapper timeline-wrapper-success">
                        <div class="timeline-badge"></div>
                        <div class="timeline-panel">
                          <div class="timeline-heading">
                            <h6 class="timeline-title"> Sinkron Data dari Online ke Offline</h6>
                          
                        </div>
                      </div>
                    </div>

                      <div class="timeline-wrapper timeline-inverted timeline-wrapper-info">
                        <div class="timeline-badge"></div>
                        <div class="timeline-panel">
                          <div class="timeline-heading">
                            <h6 class="timeline-title"> Login aplikasi Offline </h6>
                          </div>
                        </div>
                      </div>
					  
                      
                    </div>
         
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 . All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal </span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

 <?php $this->load->view('footer.php'); ?>
</body>

</html>