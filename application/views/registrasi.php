<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Rapor</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/simple-line-icons/css/simple-line-icons.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/flag-icon-css/css/flag-icon.min.css">

   <link rel="stylesheet" href="<?php echo base_url();?>assets/node_modules/perfect-scrollbar/css/perfect-scrollbar.css">
    <link rel="stylesheet" type="<?php echo base_url();?>assets/node_modules/sweetalert2/dist/sweetalert2.min.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/logo_kemenag.png">
</head>

<body>
  <div class="container-scroller">
     <?php echo $this->session->flashdata('msg3');?>
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth" style="
    background-color: background: #1e5799; /* Old browsers */
    background: -moz-linear-gradient(left, #1e5799 0%, #ce27b5 0%, #ad56a0 11%, #ad56a0 28%, #f91d42 100%, #7db9e8 100%, #f91d42 102%); /* FF3.6-15 */
    background: -webkit-linear-gradient(left, #1e5799 0%,#ce27b5 0%,#ad56a0 11%,#ad56a0 28%,#f91d42 100%,#7db9e8 100%,#f91d42 102%); /* Chrome10-25,Safari5.1-6 */
    background: linear-gradient(to right, #1e5799 0%,#ce27b5 0%,#ad56a0 11%,#ad56a0 28%,#f91d42 100%,#7db9e8 100%,#f91d42 102%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1e5799', endColorstr='#f91d42',GradientType=1 ); /* IE6-9 */
">
<style type="text/css">
  .count-indicator .count {
    position: absolute;
    left: 46%;
    width: 17px;
    height: 17px;
    top: 10px;
    color: #ffffff;
    border-radius: 50px;
    text-align: center;
    line-height: 1;
    padding: 3px 7px 1px 6px;
    font-size: 0.75rem;
}
</style>
        <div class="row w-100">
          <div class="col-lg-9 mx-auto">
            <div class="auth-form-light text-left p-5">
              <h2>Register</h2>
                <form action="<?php echo site_url('registrasi/aksi_insert/');?>" method="post" enctype="multipart/form-data">
                  <div class="form-group row">
                       <label style="padding-top: 20px;" class="col-sm-3 col-form-label">Nama RA</label>
                          <div class="col-sm-9">
                          <input required type="text" name="namaS" class="form-control form-control-lg inputtxt" onkeyup="validhuruf(this)"></div>
                      </div>
                    <div class="form-group row">
                       <label style="padding-top: 20px;" class="col-sm-3 col-form-label">NSM</label>
                          <div class="col-sm-9"><input type="number" name="nsn" class="form-control form-control-lg inputtxt" minlength="10" onkeyup="validAngka(this)" required><?php echo $this->session->flashdata('message1');?></div>
                      </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Alamat</label>
                  </div>
                   <div class="form-group row" >
                       <label  class="col-sm-3 col-form-label"> &emsp;&emsp;&emsp;Jalan</label>
                          <div class="col-sm-9">
                          <input type="text" name="jln" class="form-control form-control-lg inputtxt" onkeyup="validhuruf(this)">
                           </div>
                           </div>
                          <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label"> &emsp;&emsp;&emsp;Kelurahan/Desa/Dusun</label>
                          <div class="col-sm-9">
                          <input type="text" name="kel" class="form-control form-control-lg inputtxt" onkeyup="validhuruf(this)">
                           </div>
                           </div>
                            <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label"> &emsp;&emsp;&emsp;Kecamatan</label>
                          <div class="col-sm-9">
                          <input type="text" name="kec" class="form-control form-control-lg inputtxt" onkeyup="validhuruf(this)">
                           </div>
                           </div>
                           <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label"> &emsp;&emsp;&emsp;Kode POS</label>
                          <div class="col-sm-9">
                          <input type="text" name="pos" class="form-control form-control-lg inputtxt" onkeyup="validAngka(this)">
                           </div>
                           </div>
                            <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label"> &emsp;&emsp;&emsp;Provinsi</label>
                          <div class="col-sm-9">
                         <select required name="prov" class=" form-control form-control-sm" id="prov" onchange="get_kota()">
                           <option value=""> Pilih Provinsi</option>
                             <?php
                            foreach($prov as $r){
                            echo "<option value='".$r->id_provinsi."'>".$r->nama_provinsi."</option>";}
                        ?> 
                         </select>
                           </div></div>
                             <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label"> &emsp;&emsp;&emsp;Kota/Kabupaten</label>
                          <div class="col-sm-9" id="div_kota">
                          <select required name="kab" class=" form-control form-control-sm">
                           <option> Pilih kabupaten</option>
                         </select>
                           </div>
                           </div>

                            <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label"> Web site</label>
                          <div class="col-sm-9">
                          <input type="text" name="web" class="form-control form-control-lg inputtxt" onkeyup="validhuruf(this)">
                           </div>
                           </div>
                            <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label"> Telpon</label>
                          <div class="col-sm-9">
                          <input type="number" name="tlp" class="form-control form-control-lg"  onkeyup="validAngka(this)">
                           </div>
                           </div>
                            <div class="form-group row" >
                          <label  class="col-sm-3 col-form-label"> Email</label>
                          <div class="col-sm-9">
                          <input required type="email" name="email" class="form-control form-control-lg inputtxt" >
                           <p class="" style="color: red;">*PASTIKAN EMAIL YANG DIMASUKAN VALID.</p>
                           </div>
                           </div>

                            <div class="form-group row">
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Nama Kepala RA</label>
                          <div class="col-sm-9">
                           <input required type="text" name="kepRA" class="form-control form-control-lg inputtxt" onkeyup="validhuruf(this)">
                           </div>
                      </div>
                        <div class="form-group row">
                       <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Tahun Ajaran</label>
                          <div class="col-sm-9">
                           <input  type="text" name="thn" class="form-control form-control-lg inputtxt" required onkeyup="validtahun(this)">
                            <p class="d-inline ml-3 text-muted" >cth 2018/2019.</p>
                           <input type="hidden" name="tgl" class="form-control"
                                    value="<?php $tanggal=date('Y-m-d');echo $tanggal;?>">
                           </div>
                      </div>
                     
                 
                  <div class="mt-5">
                    <button type="submit" class="btn btn-block btn-primary btn-lg font-weight-medium">Register</button>
                    
                  </div>
                             </form>                  
             
            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
 <?php $this->load->view('footer.php'); ?>
 <script src="<?php echo base_url();?>assets/node_modules/sweetalert/dist/sweetalert.min.js"></script>
   <script src="<?php echo base_url();?>assets/js/alerts.js"></script>
 <script>
            function get_kota(){
                var id_provinsi = $("#prov").val();
                $.ajax({ 
                    type: 'POST', 
                    url: "<?php echo site_url('Registrasi/get_kota'); ?>", 
                    data:"id_provinsi="+id_provinsi, 
                    success: function(msg) {
                            $("#div_kota").html(msg);
                    }
                });
            }
        </script>
		<script language='javascript'>
function validAngka(a)
{
  if(!/^[0-9.]+$/.test(a.value))
  {
  a.value = a.value.substring(0,a.value.length-10);
   var valid = /^d{0,15}(.d{0,2})?$/.test(this.value),
        val = this.value;
  }
  if(!valid){
        console.log("Invalid input!");
        this.value = val.substring(0, val.length - 1);
    }
}

function validhuruf(a)
{
  if(!/^[a-zA-Z'0-9. ]+$/.test(a.value))
  {
  a.value = a.value.substring(0,a.value.length-10);
   var valid = /^d{0,15}(.d{0,2})?$/.test(this.value),
        val = this.value;
  }
  if(!valid){
        console.log("Invalid input!");
        this.value = val.substring(0, val.length - 1);
    }
}


</script>
<!-- <script type="text/javascript"> 
function killCopy(e){
  return false
} function reEnable(){
  return true
} document.onselectstart=new Function(return false){
  if (window.sidebar){
    document.onmousedown=killCopy;
    document.onclick=reEnable;
  }
} </script> -->
<script type="text/javascript">
   $('.inputtxt').bind("cut copy paste",function(e) {
     e.preventDefault();
 });
</script>
    <script language='javascript'>
function validtahun(a)
{
  if(!/^[0-9/]+$/.test(a.value))
  {
  a.value = a.value.substring(0,a.value.length-10);
   var valid = /^d{0,15}(.d{0,2})?$/.test(this.value),
        val = this.value;
  }
  if(!valid){
        console.log("Invalid input!");
        this.value = val.substring(0, val.length - 1);
    }
}
</script>
</body>

</html>
