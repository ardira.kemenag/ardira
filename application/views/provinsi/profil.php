<!DOCTYPE html>
<html lang="en">
<?php require_once('header.php'); 
$role=$this->session->userdata('role');
if($this->session->userdata('id_akun')!=0){
    $username=$this->session->userdata('username');
    $password=$this->session->userdata('passsword');
    $nama=$akun->nama;
    $nrp=$akun->nrp;
     //$title='Ubah';
     $aksi='operator/update_profil/';
  }else{
    $nama='';
    $username=$this->session->userdata('username');
    $password=$this->session->userdata('passsword');
    $nama='';
    $nrp='';
   // $title='Tambah';
    $aksi='operator/update_profil';
  }
  
 
  ?>
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/node_modules/select2/dist/css/select2.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/node_modules/select2-bootstrap-theme/dist/select2-bootstrap.min.css">
<body>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
           <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
               <div class="card-body">
              <h4 class="card-title"> Data Akun ?>
                 </h4>
                  <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb bg-light">
                        <li class="breadcrumb-item"><a href="<?php echo site_url('Admin/dashboard');?>">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page"> Data Akun</li>
                      </ol>
                    </nav>
                <form method="post" action="<?php echo site_url($aksi);?>"  >
                
                  <?php if($this->session->userdata('id_akun')!=0)  {?>
                  <div class="row form-group">
                     <div class="col-lg-3">
                      <label class="col-form-label">Kesatuan</label>
                    </div>
                    <div class="col-lg-9">
                         <select required class="js-example-basic-single" name="kesatuan" style="width:100%"  >
                             <?php
                             foreach ($kesatuan->result() as $k) {
                            ?>
                         <option value="<?php echo $k->id_kesatuan; ?>" <?php if($this->uri->segment(3)){
                                        if($akun->kesatuan==$k->id_kesatuan)echo "selected"; }?>> <?php echo $k->nama_kesatuan; ?>
                                      </option>
                         <?php } ?> 
                          </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-lg-3">
                      <label class="col-form-label">Nama</label>
                    </div>
                    <div class="col-lg-9">
                      <input type="text" name="nama" class="form-control form-control-lg" value="<?php echo $nama?>">
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-lg-3">
                      <label class="col-form-label">NRP</label>
                    </div>
                    <div class="col-lg-9">
                      <input type="text" name="nrp" class="form-control form-control-lg" value="<?php echo $nrp?>">
                    </div>
                  </div>
                 
                  <div class="form-group row">
                    <div class="col-lg-3">
                      <label class="col-form-label">Biro</label>
                    </div>
                    <div class="col-lg-9">
                      <select required name='role' class="form-control form-control-sm">
                        <option value="2">Provos</option>
                        <option value="3">Wabprof</option>
                      </select>
                    </div>
                  </div>
                <?php }?>
                  <div class="form-group row">
                    <div class="col-lg-3">
                      <label class="col-form-label">username</label>
                    </div>
                    <div class="col-lg-9">
                      <input type="text" name="username" class="form-control form-control-lg" value="<?php echo $username?>">
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-lg-3">
                      <label class="col-form-label">password</label>
                    </div>
                    <div class="col-lg-9">
                      <input type="password" name="password" class="form-control form-control-lg" value="">
                    </div>
                     <input type="hidden" name="passwordold" class="form-control form-control-lg" value="<?php echo $password?>">
                  </div>
                 
                
                       
                     
                        <div class="col-md-12 text-right">
                        <button type="submit" id="idSubmit" class="btn btn-inverse-primary btn-fw ">Submit</button>
                        <a href="<?php echo site_url(''); ?>" class="btn btn-inverse-danger btn-fw">Batal</a>
                                </div> 
                        <br>
                </form>
                </div>
              </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 . All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">POLRI</span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

 <?php $this->load->view('footer.php'); ?>
 
   <script src="<?php echo base_url();?>assets/typeahead.min.js"></script>
 <script>
    $(document).ready(function(){
     
    $('input.typeahead').typeahead({
        name: 'typeahead',
        remote:'<?php echo site_url('Operator/cekpos');?>?key=%QUERY',
        limit : 10
    });
});
    $("#tampil").change(function (){

                var id = $('#tampil').val();

                if (id=="3" ){
                  $("#tampil_detail").show();
                }
                
                else{
                  $("#tampil_detail").hide();
                }

              });
    </script>

</body>



</html>