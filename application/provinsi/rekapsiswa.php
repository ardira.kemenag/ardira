<!DOCTYPE html>
<html lang="en">
<?php require_once('header.php'); 
  ?>

<body>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row user-profile"><?php
                                  $datasiswa = $dtRA->result();
                                  ?>
          

            <div class="col-lg-12 side-right stretch-card">
              <div class="card">
                <div class="card-body">
                  <div class="wrapper d-block d-sm-flex align-items-center justify-content-between">
                    <h4 class="card-title mb-0">Details</h4>
<!--                     <ul class="nav nav-tabs tab-solid tab-solid-primary mb-0" id="myTab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" id="info-tab" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-expanded="true">Info</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="avatar-tab" data-toggle="tab" href="#avatar" role="tab" aria-controls="avatar">Siswa</a>
                      </li>
                     
                    </ul> -->
                  </div>
                  <div class="wrapper">
                    <hr>
                    <div class="tab-content" id="myTabContent">
                      <div class="tab-pane fade show active" id="info" role="tabpanel" aria-labelledby="info">
                        <form action="<?php echo site_url('kepalaRA/aksi_ubahfoto/')?>" method="post" enctype="multipart/form-data">
                        </form>
                        <table id="mytable" class="table table-striped table-advance table-hover">
                           <thead>
                      <tr> 
                         <th>No</th>
                         <th><center>Nama</center></th>
                        <th><center>Tahun ajaran</center></th>
                        <th><center>Kelompok</center></th>
                        <th><center>Aksi</center></th>
                        
                      </tr>
                    </thead>
                         
                 <tbody>
                        <?php
                        $no = 1;
                        foreach($siswa->result() as $b){
                        ?>
                        <tr>
                    <td><center> <?php echo $no++ ?></center></td>
                    <td><center> <?php echo $b->nama_siswa ?></center></td>
                    <td><center> <?php echo $b->nama_tahun?></center></td>
                   <td><center> <?php echo $b->kelompok?></center></td>
                     <td>  <a href='#' class='edit-record btn btn-primary btn-sm' data-id='<?php echo $b->nama_siswa?>'> <i class='icon-eye'></i> lihat</a></td>
                      </tr>
                                <?php
                                    }
                                ?>
                  </tbody>
                        </table>
                       <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          <table id="lookup" class="table table-bordered table-hover table-striped" style="background-color:white;">
                            <thead>
                                <tr>
                                  <th> Program</th>
                                  <th>Minggu</th>
                                </tr>
                            </thead>
                            <tbody>
                   
                            </tbody>

                        </table>
                        </div>
                        <div class="modal-footer">
                          
                          
                          <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
                        </div>
                      </div>
                    </div>
                  </div>
                      </div><!-- tab content ends -->
                   
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018  All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Raudhatul Athfal </span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
     

 <?php $this->load->view('footer.php'); ?>
  <script>
            $(function () {
                $(document).on('click', '.edit-record', function (e) {
                    e.preventDefault();
                    $("#exampleModal").modal('show');
                    $.post('<?php echo site_url('Rekap/nilaisiswa');?>',
                            {id: $(this).attr('data-id')},
                    function (html) {
                        $(".modal-body").html(html);
                    }
                    );
                });
            });
        </script>
</body>

</html>