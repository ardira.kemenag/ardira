<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registrasi extends CI_Controller {

	public function __construct(){
			parent::__construct();
			$this->load->model('model_m');
			 $this->load->library('email');
			  $this->load->library('user_agent');
            date_default_timezone_set("Asia/Jakarta");
            
           
          }
	public function index()

	{
		$data['prov']=$this->model_m->selectsemua('provinsi')->result();
		$this->load->view('registrasi',$data);
	}
 	public function validasi()

	{
		 $data['jmlreq']=$this->model_m->selectx('sekolah','status='.'0')->num_rows();
		 $data['jmlsekolah']=$this->model_m->selectx('sekolah','status='.'0')->num_rows();
		$data['skl']=$this->model_m->rarequest();
		$this->load->view('pusat/validasi',$data);
	}
	public function validasikanwil()

	{

		$id_provinsi=$this->session->userdata('id_provinsi');
		 $data['jmlsekolah']=$this->model_m->selectx('sekolah','status='.'0' and 'id_provinsi='.$id_provinsi)->num_rows();
		 $data['jmlreq']=$this->model_m->selectX('sekolah',"status=0 and id_provinsi ='".$this->session->userdata('id_provinsi')."'")->num_rows();
		$data['skl']=$this->model_m->rakanwil($id_provinsi);
		$this->load->view('provinsi/validasi',$data);
	}
 
 	function get_kota(){
        $this->load->model('model_m');
        $id_provinsi = $this->input->post('id_provinsi');
        $dataKota = $this->model_m->getDataKota1($id_provinsi);
        
        echo '<select required name="kab" class="form-control form-control-sm" id="kota">
        <option value="">PILIH KOTA DAN KABUPATEN</option>';

        foreach($dataKota as $a){
            echo '
            <option value="'.$a->id_kotaKab.'">'.$a->nama_kota.'</option>';
        }
        echo '</select>';
    }
    public function aksi_insert()

	{
			$this->form_validation->set_rules(
        'nsn', 'NSN',
        'required|min_length[8]|max_length[16]|is_unique[sekolah.NSN]',
        array(
                'required'      => 'You have not provided %s.',
                'is_unique'     => 'Data %s sudah ada.'
        )
);
	$valid1 =  $this->form_validation->run();
        $pesan1 = validation_errors();
         $this->form_validation->reset_validation();
//////////////valid 2/////////////////////
        
          if($valid1 == false  ){
             if ($valid1 == false){
                 echo $this->session->set_flashdata('message1', "
                 	<div class='alert alert-danger'>
           <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button><h4><i class='mdi mdi-alert-circle'></i> Perhatian!!</h4>
          ".$pesan1." </div>");
         }else{
                echo $this->session->set_flashdata('message1',"");
             }
   
             
          $config['upload_path'] = './uploads';
		$config['allowed_types'] = 'jpg|png|jpeg|pdf';		
			
		$this->upload->initialize($config);
		if($this->upload->do_upload('foto')){
			$data=$this->upload->data();			
			//$config['file_name'];
			$foto	= $data["file_name"];				
		}else{ $foto	=''; }
		$data1 = array(
							'nama_sekolah'=>$this->input->post('namaS'),
							'NSN'=>$this->input->post('nsn'),
							'jalan'=>$this->input->post('jln'),
							'kelurahan'=>$this->input->post('kel'),
							'kecamatan'=>$this->input->post('kec'),
							'kode_pos'=>$this->input->post('pos'),
							'id_provinsi'=>$this->input->post('prov'),
							'id_kotaKab'=>$this->input->post('kab'),
							'website'=>$this->input->post('web'),
							'telepon'=>$this->input->post('tlp'),
							'email'=>$this->input->post('email'),
							'kepala_RA'=>$this->input->post('kepRA'),
							'tahun_ajaran'=>$this->input->post('thn'),
							'tgl_daftar'=>$this->input->post('tgl'),
							//'foto'=>$foto,
						);
					
					  
             $this->load->view('registrasi', $data1);

		}else{
			$config['upload_path'] = './uploads';
		$config['allowed_types'] = 'jpg|png|jpeg|pdf';		
			
		$this->upload->initialize($config);
		if($this->upload->do_upload('foto')){
			$data=$this->upload->data();			
			//$config['file_name'];
			$foto	= $data["file_name"];				
		}else{ $foto	=''; }
		$data1 = array(
							'nama_sekolah'=>$this->input->post('namaS'),
							'NSN'=>$this->input->post('nsn'),
							'jalan'=>$this->input->post('jln'),
							'kelurahan'=>$this->input->post('kel'),
							'kecamatan'=>$this->input->post('kec'),
							'kode_pos'=>$this->input->post('pos'),
							'id_provinsi'=>$this->input->post('prov'),
							'id_kotaKab'=>$this->input->post('kab'),
							'website'=>$this->input->post('web'),
							'telepon'=>$this->input->post('tlp'),
							'email'=>$this->input->post('email'),
							'kepala_RA'=>$this->input->post('kepRA'),
							'tahun_ajaran'=>$this->input->post('thn'),
							'tgl_daftar'=>date('Y-m-d H:i:s'),
							//'foto'=>$foto,
						);
					$query=$this->model_m->input_data('sekolah',$data1);
	 $id= $this->db->insert_id($query);
	 if ($this->agent->is_browser())
      {
        $agent = $this->agent->browser();
      }
      elseif ($this->agent->is_robot())
      {
        $agent = $this->agent->robot();
      }
      elseif ($this->agent->is_mobile())
      {
         $agent = $this->agent->mobile();
      }
      else
      {
        $agent = 'Unidentified User Agent';
      }
      
      $sekolah=$id;
      $browser_log    = $agent;
      $browser_version  = $this->agent->version();
      $platform_log     = $this->agent->platform(); 
      $ip_log       = $this->input->ip_address();
        $data = array(
        'id_sekolah'=>$sekolah,
        'browser_log'=>$browser_log,
        'browser_version'=>$browser_version,
        'platform_log'=>$platform_log,
        'ip_log'=>$ip_log,
        'date_login'=>date('Y-m-d H:i:s')
      );
      $id_log = $this->db->insert('user__logs_regis',$data);
				  echo $this->session->set_flashdata('msg3', '
                 	<div class="swal-overlay swal-overlay--show-modal" tabindex="-1">
				  <div class="swal-modal" role="dialog" aria-modal="true"><div class="swal-icon swal-icon--success">
				  <span class="swal-icon--success__line swal-icon--success__line--long"></span>
				  <span class="swal-icon--success__line swal-icon--success__line--tip"></span>

				    <div class="swal-icon--success__ring"></div>
				    <div class="swal-icon--success__hide-corners"></div>
				  </div><div class="swal-title" style="">Registrasi Berhasil!</div><div class="swal-text" style="">Tunggu Konfirmasi selanjutnya via email</div> <div class="swal-footer"><div class="swal-button-container">

				    <a class="swal-button swal-button--confirm btn btn-primary" href="'.site_url('Login').'">Continue</a>

				    <div class="swal-button__loader">
				      <div></div>
				      <div></div>
				      <div></div>
				    </div>

  				</div></div></div></div>');
				 redirect('registrasi');

		}

		
	}
		public function popup(){

        	$id=$_POST['id'];
        	
       		$data['hasil']=$this->model_m->detailRA($id)->result();

       		$data['kode'] = $this->model_m->getpassword();
			$this->load->view('pusat/showRA',$data);
           
            
        }
          public function approve($id_sekolah){
		$where = array('id_sekolah'=>$this->input->post('fid'));
		$status = array('status'=>'1',
						'verifikator'=>$this->session->userdata('id_user'),
						'tgl_verif'=>date('Y-m-d H:i:s'),
					);

		$data1 = array(

							'username'=>$this->input->post('username'),
							'password'=>$this->input->post('password'),
							'id_sekolah'=>$this->input->post('fid'),
							'id_role'=>$this->input->post('role'),
							'status'=>$this->input->post('status'),
							
							//'foto'=>$foto,
						);
					$this->model_m->input_data('user',$data1);

		$update = $this->model_m->update_data('sekolah',$status,$where);
		// $to_email = 'indah.rsydh17@gmail.com';
		$username = $this->input->post('username');
		$to_mail = $this->input->post('email');
    	$password = $this->input->post('password');
		 $this->load->library('email');
    $from_email = "no-reply";

    $config['protocol']    = 'smtp';
    $config['smtp_host']    = 'ssl://smtp.gmail.com';
    $config['smtp_port']    = '465';
    $config['smtp_timeout'] = '7';
    $config['smtp_user']    = 'ardira.kemenag@gmail.com';
    $config['smtp_pass']    = 'ardirarapor';
    $config['charset']    = 'utf-8';
    $config['newline']    = "\r\n";
    $config['mailtype'] = 'html'; // or html
    $config['validation'] = TRUE; // bool whether to validate email or not

    $this->email->initialize($config);

    $this->email->from($from_email, 'Kementrian Agama');
    $this->email->to($to_mail);
    $this->email->subject('Konfirmasi');

    // $base = base_url();
    $this->email->message('
          <h2>EMAIL KONFIRMASI Registrasi RA</h2>
          <h4> Selamat Ra Anda telah terdaftar pada sistem untuk penggunaan aplikasi rapor digital</h4>
          <p> Silakan login dengan menggunakan :
          <br>  <p>Username : <strong>'.$username.'</strong>
          <br>  <p>Password : <strong>'.$password.'</strong></p>

          <p>
          Terima Kasih<br>
		  Kementrian Agama RI</p>
          
          
    ');

    //Send mail
    $this->email->send();
		if($this->session->userdata('id_provinsi')=='0'){
		redirect('Registrasi/validasi',$data);
		}
		else{
			redirect('Registrasi/validasikanwil',$data);
		}
	}
	public function reject($id_sekolah){
            $where = array("id_sekolah"=>$id_sekolah);
            $this->model_m->delete_data('sekolah',$where);
			//$this->menu_m->delete_data('menu',$where);
			if($this->session->userdata('id_provinsi')=='0'){
		redirect('Registrasi/validasi');
		}
		else{
			redirect('Registrasi/validasikanwil');
		}
            
        }
	

	
	
	
}
