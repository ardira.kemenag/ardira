<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
			parent::__construct();
			$this->load->model('model_m');
      if(!$this->session->userdata('username')){
          redirect('login');
       }
           
          }
	public function index()

	{
       $id_sekolah=$this->session->userdata('id_sekolah');
    $data['jmlguru']=$this->model_m->guru($id_sekolah)->num_rows();
    $data['jmlsiswa']=$this->model_m->datasiswa($id_sekolah)->num_rows();
    $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
    $this->load->view('home',$data);
	}
    public function data()

  {
    $id_sekolah=$this->session->userdata('id_sekolah');
   
    $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
    $data['data']= $this->model_m->terkirim();
      $this->load->view('datarekap',$data);
  }
	public function dashboard()

	{
    $id_sekolah=$this->session->userdata('id_sekolah');
    $data['jmlguru']=$this->model_m->guru($id_sekolah)->num_rows();
    $data['jmlsiswa']=$this->model_m->datasiswa($id_sekolah)->num_rows();
    $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		$this->load->view('home',$data);
	}
  public function rkp($id_sekolah,$id_tahunajaran)

  {
  
    $where = array('id_sekolah' => $id_sekolah,'id_tahunajaran' => $id_tahunajaran);
    $data['rkp']= $this->model_m->selectX('coba_rekap',$where);
    $data['nama']= $this->model_m->nama($id_tahunajaran);
   $this->load->view('pusat/rekapbulanan',$data);
  }
 
	


    public function aksi_insert()

  {
   
    $data1 = array(
              'id_program'=>$this->input->post('f1'),
              'jam'=>$this->input->post('f2'),
              'hari'=>$this->input->post('f4'),
             'id_kelas'=>$this->input->post('f3'),
            );
          $this->model_m->input_data('jadwal',$data1);
            redirect('home/penjadwalan');
  }
   public function terkirim()

  {
    $id_sekolah=$this->session->userdata('id_sekolah');
   
    $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
    $data['data']= $this->model_m->terkirimsekolah($id_sekolah);
      $this->load->view('sekolah/datarekap',$data);
  }
   
	
}
