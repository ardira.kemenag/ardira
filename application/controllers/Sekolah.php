<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sekolah extends CI_Controller {

	public function __construct(){
			parent::__construct();
			$this->load->model('model_m');
			$this->load->library('PHPExcelS/Classes/PHPExcel');
            $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
			if(! $this->session->userdata('username')){
				 redirect('login');
			}
           
          }
	public function index()

	{
		 $data['jmlreq']=$this->model_m->selectx('sekolah','status='.'1')->num_rows();
		$data['prov']=$this->model_m->selectsemua('provinsi')->result();
		if($this->session->userdata('role')==3){
		$this->load->view('pusat/tambahsekolah',$data);	
		}
		else{
			$this->load->view('provinsi/tambahsekolah',$data);	
		}
		
	}
 
 	public function school()

	{
		 $data['jmlreq']=$this->model_m->selectx('sekolah','status='.'0')->num_rows();
		$data['skl']=$this->model_m->sekolah();
		$data['prov']=$this->model_m->selectsemua('provinsi')->result();
		$this->load->view('pusat/sekolah',$data);
	}
	public function kanwilschool()

	{
		$data['jmlreq']=$this->model_m->selectX('sekolah',"status=0 and id_provinsi ='".$this->session->userdata('id_provinsi')."'")->num_rows();
		if($this->input->post()){
			$prov=$this->session->userdata('id_provinsi');
			$kab=$this->input->post('kabupaten');
		$data['skl']= $this->model_m->sekolah_filter2($prov,$kab);
		}else{
		$id_provinsi=$this->session->userdata('id_provinsi');
		$where = array('id_provinsi' => $id_provinsi );
		$data['skl']=$this->model_m->sekolahprov($id_provinsi);
		}
			$data['prov']=$this->model_m->selectsemua('provinsi')->result();

       $data['kab'] = $this->model_m->getDataKota1($this->session->userdata('id_provinsi'));
		$this->load->view('provinsi/sekolah',$data);
	}
		
function get_kota(){
        $this->load->model('model_m');
        $id_provinsi = $this->input->post('id_provinsi');
        $dataKota = $this->model_m->getDataKota1($id_provinsi);
        
        echo '<select required name="kab" class="form-control form-control-sm" id="kota" required>
        <option value="">Show All Kota/Kabupaten</option>';

        foreach($dataKota as $a){
            echo '
            <option value="'.$a->id_kotaKab.'">'.$a->nama_kota.'</option>';
        }
        echo '</select>';
    }
    function get_kota2(){
        $this->load->model('model_m');
        $id_provinsi = $this->input->post('id_provinsi');
        $dataKota = $this->model_m->getDataKota1($id_provinsi);
        
        echo '<option value="0">AllKota/Kabupaten</option>';

        foreach($dataKota as $a){
            echo '
            <option value="'.$a->id_kotaKab.'">'.$a->nama_kota.'</option>';
        }
    }
    public function sekolah_search(){
    	$prov= $this->input->post('provinsi');
    	$kab= $this->input->post('kabupaten');
    	$data['jmlreq']=$this->model_m->selectx('sekolah','status='.'0')->num_rows();
    	if($kab==0){
    		$data['skl']=$this->model_m->sekolah_filter($prov);
    	}else{
    		$data['skl']=$this->model_m->sekolah_filter2($prov,$kab);
    	}
		
		$data['prov']=$this->model_m->selectsemua('provinsi')->result();
		$data['kab']=$this->model_m->selectX('kota_kab','id_provinsi='.$prov)->result();
		$this->load->view('pusat/sekolah',$data);

    }
     public function cetak(){
     	//$this->load->library('PHPExcelS/classes/PHPExcel');
     	if($this->uri->segment(3)){
     		$prov=$this->uri->segment(3);
     		$kab=$this->uri->segment(4);
			if($this->uri->segment(4)==0){
				$data['skl']=$this->model_m->sekolah_filter($prov)->result();
				$data['data']=$this->model_m->selectsemua('provinsi')->result();
			}else{
				$data['skl']=$this->model_m->sekolah_filter2($prov,$kab)->result();
				$data['data']=$this->model_m->selectsemua('provinsi')->result();
			}
     	}else{
     		$data['skl']=$this->model_m->sekolah_all()->result();
     		$data['data']=$this->model_m->selectsemua('provinsi')->result();
     	}
    	
		$this->load->view('pusat/cetak_excel',$data);

    }

    public function aksi_insert()

	{
		$config['upload_path'] = './uploads';
		$config['allowed_types'] = 'jpg|png|jpeg|pdf';		
			
		$this->upload->initialize($config);
		if($this->upload->do_upload('foto')){
			$data=$this->upload->data();			
			//$config['file_name'];
			$foto	= $data["file_name"];				
		}else{ $foto	=''; }
		$data1 = array(
							'nama_sekolah'=>$this->input->post('namaS'),
							'NSN'=>$this->input->post('nsn'),
							'jalan'=>$this->input->post('jln'),
							'kelurahan'=>$this->input->post('kel'),
							'kecamatan'=>$this->input->post('kec'),
							'kode_pos'=>$this->input->post('pos'),
							'id_provinsi'=>$this->input->post('prov'),
							'id_kotaKab'=>$this->input->post('kab'),
							'website'=>$this->input->post('web'),
							'telepon'=>$this->input->post('tlp'),
							'email'=>$this->input->post('email'),
							'kepala_RA'=>$this->input->post('kepRA'),
							'tahun_ajaran'=>$this->input->post('thn'),
							//'foto'=>$foto,
						);
					$this->model_m->input_data('sekolah',$data1);
					  redirect('sekolah/school');
	}
	public function detail($id_sekolah)

	{
		$data['jmlreq']=$this->model_m->selectx('sekolah','status='.'0')->num_rows();

		$where = array('id_sekolah' => $id_sekolah );
	
            $data['dtRA'] = $this->model_m->detailakun($id_sekolah);
            $data['siswa'] = $this->model_m->listsiswa3($id_sekolah);
		$this->load->view('pusat/madrasahdetail',$data);
	}
	public function data($id_sekolah)

	{
		$data['jmlreq']=$this->model_m->selectx('sekolah','status='.'0')->num_rows();

		$where = array('id_sekolah' => $id_sekolah );
	
            $data['dtRA'] = $this->model_m->detailakun($id_sekolah);
            $data['siswa'] = $this->model_m->listsiswa3($id_sekolah);
		$this->load->view('pusat/madrasahdetail',$data);
	}
	public function detailkanwil($id_sekolah)

	{
		 $data['jmlreq']=$this->model_m->selectX('sekolah',"status=0 and id_provinsi ='".$this->session->userdata('id_provinsi')."'")->num_rows();

		$where = array('id_sekolah' => $id_sekolah );
	
            $data['dtRA'] = $this->model_m->detailakun($id_sekolah);
            $data['siswa'] = $this->model_m->listsiswa3($id_sekolah);
		$this->load->view('provinsi/madrasahdetail',$data);
	}
	
	
}
