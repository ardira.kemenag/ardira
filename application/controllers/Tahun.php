<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tahun extends CI_Controller {

	public function __construct(){
			parent::__construct();
			$this->load->model('model_m');
			if(! $this->session->userdata('username')){
				 redirect('login');
			}
           
          }
	public function index()

	{
		 $data['jmlreq']=$this->model_m->selectx('sekolah','status='.'0')->num_rows();
		$id_sekolah=$this->session->userdata('id_sekolah');
		$id_sekolah=$this->session->userdata('id_sekolah');
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		
		$data['guru']=$this->model_m->guru($id_sekolah);
		$this->load->view('pusat/inserttahun',$data);
	}
 
 	public function thn()

	{
		 $data['jmlreq']=$this->model_m->selectx('sekolah','status='.'0')->num_rows();
		$id_sekolah=$this->session->userdata('id_sekolah');
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		$data['kls']=$this->model_m->selectsemua('tahun_ajaran');
		$this->load->view('pusat/tahun',$data);
	}
		

	
    public function aksi_insert()

	{
		$config['upload_path'] = './uploads';
		$config['allowed_types'] = 'jpg|png|jpeg|pdf';		
			
		$this->upload->initialize($config);
		if($this->upload->do_upload('foto')){
			$data=$this->upload->data();			
			//$config['file_name'];
			$foto	= $data["file_name"];				
		}else{ $foto	=''; }
		$data1 = array(
							'nama_tahun'=>$this->input->post('f1'),
							
							
							//'foto'=>$foto,
						);
					$this->model_m->input_data('tahun_ajaran',$data1);
					  redirect('Tahun/thn');
	}
	public function ubah_tahun($id_tahun)

	{
		$where= array('id_tahunajaran' => $id_tahun );
		$id_sekolah=$this->session->userdata('id_sekolah');
		 $data['jmlreq']=$this->model_m->selectx('sekolah','status='.'0')->num_rows();
		//$id_sekolah=$this->session->userdata('id_sekolah');
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		//$data['guru']=$this->model_m->guru($id_sekolah);
		$data['kls']=$this->model_m->selectX('tahun_ajaran',$where);
		$this->load->view('pusat/ubahtahun',$data);
	}
	public function aksi_ubah($id_tahun)

	{
		$data1 = array(
							'nama_tahun'=>$this->input->post('f1'),
						);
					$where = array('id_tahunajaran'=>$this->input->post('fid'));
					
					$this->model_m->update_data('tahun_ajaran',$data1,$where);
					redirect('Tahun/thn');
	}

	
	
}
