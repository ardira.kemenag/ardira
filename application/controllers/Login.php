<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct(){
            parent::__construct();
            $this->load->model('model_m');
            $this->load->library('user_agent');
            date_default_timezone_set("Asia/Jakarta");
      // if(!$this->session->userdata('username')){
      //    redirect('home');
      // }
           
          }
    public function index()

    {
         $this->load->view('login');
    }
  
 
     public function verifikasi() {
      $username = $this->input->post('username');
    $password = $this->input->post('password');
    $data = $this->model_m->cekLogin($username,$password);   
    $row = $data->row();  

    if(!isset($row)) {
        echo"<script>
          alert('Gagal Login: Cek Username dan Password');
          history.go(-1);
          </script>"; 
      }else{
      $session = array('id_user' => $row->id_user,
              'username'=>$row->username,
              'password' => $row->password,
              'id_sekolah' => $row->id_sekolah,
			  'id_provinsi' => $row->id_provinsi,
        'id_role'=>$row->id_role);
              
      $this->session->set_userdata($session);
       if ($this->agent->is_browser())
      {
        $agent = $this->agent->browser();
      }
      elseif ($this->agent->is_robot())
      {
        $agent = $this->agent->robot();
      }
      elseif ($this->agent->is_mobile())
      {
         $agent = $this->agent->mobile();
      }
      else
      {
        $agent = 'Unidentified User Agent';
      }
      
      $user_id      = $row->id_user;
      $browser_log    = $agent;
      $browser_version  = $this->agent->version();
      $platform_log     = $this->agent->platform(); 
      $ip_log       = $this->input->ip_address();
        $data = array(
        'id_user'=>$user_id,
        'browser_log'=>$browser_log,
        'browser_version'=>$browser_version,
        'platform_log'=>$platform_log,
        'ip_log'=>$ip_log,
        'date_login'=>date('Y-m-d H:i:s')
      );
      $id_log = $this->db->insert('user__logs',$data);
      $log= $this->db->insert_id($id_log);
      
      $session=array('id_log'=>$log);
        if ($row->status=='1' ){
        if ($row->id_role=='1') {
        redirect('Home');
        }else if ($row->id_role=='2'){
        redirect('Pusat');
        
      }
      else if ($row->id_role=='3'){
        redirect('KepalaRA');
        
      }
	  else if ($row->id_role=='4'){
        redirect('Kanwil');
        
      }
      
  }
  else {
    echo"
      <script>
      alert('Gagal Login: akun anda belum terdaftar');
      history.go(-1);
      </script>";
  }

     
      } 

    }


   public function logout()
    {
        // delete cookie dan session
        //delete_cookie('rapor');
      date_default_timezone_set('Asia/Jakarta');
    $where = array('id_log'=>$this->session->userdata('log'));
    
    $status = array('date_logout'=>date('Y-m-d H:i:s'));
    $update = $this->model_m->update_data('user__logs',$status,$where);
        $this->session->sess_destroy();
        redirect('login');
    }
    
}
