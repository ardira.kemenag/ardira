<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KepalaRA extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('model_m');
	 
	}
	
	public function index()
	{
		$id_sekolah=$this->session->userdata('id_sekolah');
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		$data['jmlguru']=$this->model_m->guru($id_sekolah)->num_rows();
    $data['jmlsiswa']=$this->model_m->datasiswa($id_sekolah)->num_rows();
		$this->load->view('sekolah/home',$data);
	}
	public function detailRA()
	{
		$id_sekolah=$this->session->userdata('id_sekolah');
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		$data['prov']=$this->model_m->selectsemua('provinsi')->result();
		$data['kota']=$this->model_m->selectsemua('kota_kab')->result();
		$data['kelas']=$this->model_m->datakelas($id_sekolah)->result();
		$data['thn']=$this->model_m->selectsemua('tahun_ajaran');
		 	$data['siswa']=$this->model_m->listsiswa($id_sekolah);
            $data['dtRA'] = $this->model_m->detailRA($id_sekolah);
            $data['RA'] = $this->model_m->selectX('sekolah','id_sekolah='.$id_sekolah)->row();
		$this->load->view('sekolah/madrasahdetail',$data);
	}
	public function aksi_ubah($id_sekolah)

	{
	  $data['RA'] = $this->model_m->selectX('sekolah','id_sekolah='.$id_sekolah)->row();
      $datasiswa=array(
            	'nama_sekolah'=>$this->input->post('f1'),
							'NSN'=>$this->input->post('f2'),
							'jalan'=>$this->input->post('jln'),
							'kelurahan'=>$this->input->post('kel'),
							'kecamatan'=>$this->input->post('kec'),
							'kode_pos'=>$this->input->post('pos'),
							'id_provinsi'=>$this->input->post('prov'),
							'id_kotaKab'=>$this->input->post('kab'),
							'website'=>$this->input->post('web'),
							'telepon'=>$this->input->post('tlp'),
							'email'=>$this->input->post('email'),
							'kepala_RA'=>$this->input->post('kepRA'),
							'tahun_ajaran'=>$this->input->post('thn'),
      );
     
     // $where = array('id_siswa'=>$this->input->post('fid'));
	 	$this->model_m->update_data('sekolah',$datasiswa,'id_sekolah='.$id_sekolah);
	 	redirect('KepalaRA/detailRA/');
	}
	public function aksi_ubahfoto($id_sekolah)
	{
	  $data['RA'] = $this->model_m->selectX('sekolah','id_sekolah='.$id_sekolah)->row();
      $foto=$this->updateGbr($id_sekolah);
      if($foto==NULL){
      $foto=$data['RA']->foto;
      }
      $datagaleri=array(
              
			  'foto'=>$foto,
      );
       $this->model_m->update_data("sekolah", $datagaleri, 'id_sekolah='.$id_sekolah);
     
        redirect('kepalaRA/detailRA/');
      
	}

	private function updateGbr($id_sekolah){
				$tipeFile=explode('.',$_FILES["foto"]["name"]);
				$tipeFile=$tipeFile[count($tipeFile)-1];
				$url=$id_sekolah.'.'.$tipeFile;
				if(in_array($tipeFile,array("png","jpg","PNG","jpeg"))){
					if(is_uploaded_file($_FILES["foto"]["tmp_name"])){
							if(move_uploaded_file($_FILES["foto"]["tmp_name"], "./uploads/".$url)){
								return $url;
							}
					}
				}else{
					
				}
		}



		public function recent_download(){
              
              
			echo '<script>
			 window.location.href = "'.base_url('assets/ardira.zip').'"
			</script>'; 
			$datagaleri=array(
              
			  'id_user'=>$this->session->userdata('id_user'),
			  'id_sekolah'=>$this->session->userdata('id_sekolah'),
      );
       $this->model_m->input_data('user__download',$datagaleri);
			
		
		}

}
