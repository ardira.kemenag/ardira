<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Guru extends CI_Controller {

	public function __construct(){
			parent::__construct();
			$this->load->model('model_m');
           
          }
	public function index()

	{
		$id_sekolah=$this->session->userdata('id_sekolah');
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		$data['kode'] = $this->model_m->getkodeunik();
		$this->load->view('sekolah/tambahguru',$data);
	}
 
 	public function guru()

	{

		$id_sekolah=$this->session->userdata('id_sekolah');
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		$data['guru']=$this->model_m->dataguru($id_sekolah);
		$this->load->view('sekolah/guru',$data);
	}
	

	
    public function aksi_insert()

	{
		$data1 = array(
							'nama_guru'=>$this->input->post('f1'),
							'id_sekolah'=>$this->input->post('sekolah')
							//'foto'=>$foto,
						);
		$guru=$this->model_m->input_data('guru',$data1);
		$data2 = array(
							'username'=>$this->input->post('username'),
							'password'=>$this->input->post('password'),
							'id_role'=>$this->input->post('role'),
							'status'=>$this->input->post('status'),
							'id_sekolah'=>$this->input->post('sekolah'),
								'id_guru'=>$this->db->insert_id($guru),
							//'foto'=>$foto,
						);
					
						$this->model_m->input_data('user',$data2);
					  redirect('Guru/guru');
	}
	public function account()

	{

		$id_sekolah=$this->session->userdata('id_sekolah');
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		$data['guru']=$this->model_m->akunguru($id_sekolah);
		$this->load->view('sekolah/akun',$data);
	}
	
	
	
}
