<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
			parent::__construct();
			$this->load->model('model_m');
           
          }
	public function index()

	{
      $this->load->view('login');
	}
    public function data()

  {
    $id_sekolah=$this->session->userdata('id_sekolah');
   
    $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
    $data['data']= $this->model_m->terkirim();
      $this->load->view('datarekap',$data);
  }
	public function dashboard()

	{
    $id_sekolah=$this->session->userdata('id_sekolah');
    $data['jmlguru']=$this->model_m->guru($id_sekolah)->num_rows();
    $data['jmlsiswa']=$this->model_m->datasiswa($id_sekolah)->num_rows();
    $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		$this->load->view('home',$data);
	}
 
	 public function verifikasi() {
      $username = $this->input->post('username');
    $password = $this->input->post('password');

    $data = $this->model_m->cekLogin($username,$password);   
 
    $row = $data->row();  
 

    if(!isset($row)) {
        echo"<script>
          alert('Gagal Login: Cek Username dan Password');
          history.go(-1);
          </script>"; 
      }else{
      $session = array('id_user' => $row->id_user,
              'username'=>$row->username,
              'password' => $row->password,
              'id_sekolah' => $row->id_sekolah);
              
      $this->session->set_userdata($session);
          if ($row->status=='1' ){
        if ($row->id_role=='1') {
        redirect('Home/dashboard');
        }else if ($row->id_role=='2'){
        redirect('Pusat');
        
      }
      else if ($row->id_role=='3'){
        redirect('KepalaRA');
        
      }
      
  }
  else {
    echo"
      <script>
      alert('Gagal Login: akun anda belum terdaftar');
      history.go(-1);
      </script>";
  }

     
      } 

    }
    /*
	public function penjadwalaninsert()
  {
      $data['kls']=$this->model_m->selectsemua('kelas')->result();
  $data['prg']=$this->model_m->selectsemua('program')->result();
  $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
    $this->load->view('insertpenjadwalan',$data);
  }

	 public function penjadwalan(){

	$data['jdw']=$this->model_m->jadwal()->result();
  $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
    $this->load->view('penjadwalan',$data);
    }*/

    public function aksi_insert()

  {
   
    $data1 = array(
              'id_program'=>$this->input->post('f1'),
              'jam'=>$this->input->post('f2'),
              'hari'=>$this->input->post('f4'),
             'id_kelas'=>$this->input->post('f3'),
            );
          $this->model_m->input_data('jadwal',$data1);
            redirect('home/penjadwalan');
  }
   public function logout()
    {
        // delete cookie dan session
        //delete_cookie('rapor');
        $this->session->sess_destroy();
        redirect('beranda');
    }
	
}
