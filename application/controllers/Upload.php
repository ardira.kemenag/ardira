<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends CI_Controller {

 public function __construct(){
      parent::__construct();
      $this->load->model('model_m');
       // $this->load->model('model_m');
           $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
           $this->load->helper('url');
       if(!$this->session->userdata('username')){
         redirect('login');
      }
    }
  public function index()

  {
     $id_sekolah=$this->session->userdata('id_sekolah');
     $data['thn']=$this->model_m->selectsemua('tahun_ajaran');
    $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
    // $data['data']= $this->model_m->terkirim();
    $this->load->view('sekolah/uploadfile',$data);
  }

    public function aksi_insert()

  {
    $config['upload_path'] = './uploads';
    $config['allowed_types'] = 'xls|pdf|xlsx';    
      
    $this->upload->initialize($config);
    if($this->upload->do_upload('file')){
      $data=$this->upload->data();      
      //$config['file_name'];
      $file = $data["file_name"];       
    }else{ $file  =''; }
    $data1 = array(
              'id_sekolah'=>$this->input->post('sekolah'),
              
              'id_tahunajaran'=>$this->input->post('thn'),
              'file'=>$file
              //'foto'=>$foto,
            );
          $this->model_m->input_data('rekap_bulan',$data1);
           echo $this->upload->display_errors();
  }
   public function uploadlg(){
  $fileName = $this->input->post('file', TRUE);

  $config['upload_path'] = './uploads/'; 
  $config['file_name'] = $fileName;
  $config['allowed_types'] = 'xls|xlsx|csv|ods|ots';
  $config['max_size'] = 10000;

  $this->load->library('upload', $config);
  $this->upload->initialize($config); 
  
  if (!$this->upload->do_upload('file')) {
   $error = array('error' => $this->upload->display_errors());
   $this->session->set_flashdata('msg','Ada kesalah dalam upload'); 
   // redirect('Welcome'); 
  } else {
   $media = $this->upload->data();
   $inputFileName = 'uploads/'.$media['file_name'];
   
   try {
    $inputFileType = IOFactory::identify($inputFileName);
    $objReader = IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
   } catch(Exception $e) {
    die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
   }

   $sheet = $objPHPExcel->getSheet(0);
   $highestRow = $sheet->getHighestRow();
   $highestColumn = $sheet->getHighestColumn();

   for ($row = 1; $row <= $highestRow; $row++){  
     $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
       NULL,
       TRUE,
       FALSE);

     $data = array(
    //"id_siswa"=> $rowData[0][0],
                            "nama_siswa"=> $rowData[0][0],
                            "kelompok"=> $rowData[0][1],
                           // "bulan"=> $rowData[0][3],
                            "tahun"=> $rowData[0][2],
                            "semester"=> $rowData[0][3],
                            "program"=> $rowData[0][4],
                            "indikator"=> $rowData[0][5],
                            "nilai1"=> $this->model_m->ceknull($rowData[0][6]),
                            "nilai2"=> $this->model_m->ceknull($rowData[0][7]),
                            "nilai3"=> $this->model_m->ceknull($rowData[0][8]),
                            "nilai4"=> $this->model_m->ceknull($rowData[0][9]),
                            "nilai5"=> $this->model_m->ceknull($rowData[0][10]),
                            "nilai6"=> $this->model_m->ceknull($rowData[0][11]),
                 
                            'id_sekolah'=>$this->input->post('sekolah'),
              'id_tahunajaran'=>$this->input->post('thn'),
    );
    $this->db->insert("coba_rekap",$data);

   
   } 
  
 

  }  redirect('home/terkirim');

 } 



}
?>