<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pusat extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('model_m');

            date_default_timezone_set("Asia/Jakarta");
             $role = $this->session->userdata('id_role');
		if($role!= 2){
				 redirect('login');
			}
	 
	}
	
	public function index()
	
	{
		 $data['jmlsekolah']=$this->model_m->selectx('sekolah','status='.'1')->num_rows();
		 $data['jmlreq']=$this->model_m->selectx('sekolah','status='.'0')->num_rows();
		$this->load->view('pusat/home',$data);
	}

	public function admin()
	{
		 $data['jmlreq']=$this->model_m->selectx('sekolah','status='.'0')->num_rows();
		$where=array('id_role'=>'4');
		$data['admin']=$this->model_m->admin();
		$this->load->view('pusat/admin',$data);
	}
	public function admintambah()

	{
		$data['prov']=$this->model_m->selectSemua('provinsi');
		$this->load->view('pusat/tambahadmin',$data);
	}
	 public function aksi_insert()

	{
		
		$data1 = array(
							'username'=>$this->input->post('f1'),
							'password'=>$this->input->post('f2'),
							'id_provinsi'=>$this->input->post('prov'),
							'id_role'=>$this->input->post('role'),
							'status'=>$this->input->post('status'),
							'id_sekolah'=>'-1',
						);
					$this->model_m->input_data('user',$data1);
					  redirect('pusat/admin');
	}
	public function hapusadmin($id_user){
           $where = array("id_user"=>$id_user);
            $status=array('status'=>0,'id_role'=>0);
            $this->model_m->update_data('user',$status,$where);
			//$this->menu_m->delete_data('menu',$where);
			echo $this->session->set_flashdata('msg2', "
                 	<div class='alert alert-fill-danger' role='alert'>
                    <i class='mdi mdi-alert-circle'></i>
                    Data berhasil dihapus
                  </div>");
            redirect('pusat/admin');
        }
        public function edit_akun(){

        	 $id=$_POST['id'];
        	$where= array('id_user'=>$id);
       		$data['hasil']=$this->model_m->selectX('user',$where)->result();

       		// $data['kode'] = $this->model_m->getpassword();
			$this->load->view('pusat/showedit',$data);
            
        }
 public function aksi_edit()

	{
		$data1 = array(
							'username'=>$this->input->post('username'),
							'password'=>$this->input->post('password'),
						);
					$where = array('id_user'=>$this->input->post('user'));
					$this->model_m->update_data('user',$data1,$where);
					echo $this->session->set_flashdata('msg2', "
                 	<div class='alert alert-fill-warning' role='alert'>
                    <i class='mdi mdi-alert-circle'></i>
                    Data berhasil diubah
                  </div>");
					  redirect('pusat/admin');
	}
	public function banned($id_sekolah){

		$where=array('id_sekolah'=>$id_sekolah);
		$this->model_m->insert_into($id_sekolah);
	 	$this->model_m->insert_into_akun($id_sekolah);
	 	$data1 = array('status'=>'3',
						'id_user_delete'=>$this->session->userdata('id_user'),
						'tgl_delete'=>date('Y-m-d H:i:s')
					);
	 	$data2 = array('status'=>'3');
		$where = array('id_sekolah'=>$id_sekolah);
		$this->model_m->update_data('sekolah_delete',$data1,$where);
		$this->model_m->update_data('user__delete',$data2,$where);
	 	$this->model_m->delete_data('sekolah',$where);
	 	$this->model_m->delete_data('user',$where);
	 	echo $this->session->set_flashdata('msg2', "
                 	<div class='alert alert-fill-danger' role='alert'>
                    <i class='mdi mdi-alert-circle'></i>
                    Data berhasil dihapus
                  </div>");
 	redirect('sekolah/school');
 
	}

	public function manajemen()
	{
		 $data['jmlreq']=$this->model_m->selectx('sekolah','status='.'0')->num_rows();
		
		$data['logs']=$this->model_m->user_log();
		
		//  $postdata['akhir']=$this->uri->segment(3);
		// $config['base_url']=site_url('pusat/manajemen');
		// $config['per_page']= $postdata['awal']=10;
		// $config['cur_page']=0;
		// $config['num_link']=15;
		// $config['first_tag_open'] = '<li class="page-item ">';
		// $config['first_link'] = 'First';
		// $config['first_tag_close'] = '</li>';
		// $config['prev_link'] = 'Prev';
		// $config['prev_tag_open'] = '<li class="page-item ">';
		// $config['prev_tag_close'] = '</li>';
		// $config['cur_tag_open'] = '<li class=" page-item active"><a href>';
		// $config['cur_tag_close'] = '</a></li>';
		// $config['next_link'] = 'Next';
		// $config['next_tag_open'] = '<li class="page-item ">';
		// $config['next_tag_close'] = '</li>';
		// $config['num_tag_open'] = '<li class="page-item ">';
		// $config['num_tag_close'] = '</li>';
		// $config['last_tag_open'] = '<li class="page-item ">';
		// $config['last_link'] = 'Prev';
		// $config['last_tag_close'] = '</li>';
		// $config['total_rows']=$this->db->get('user__logs')->num_rows();

		// $this->pagination->initialize($config);
		// $data['logs']=$this->model_m->user_log($postdata);
		// $str_link= $this->pagination->create_links();
		// $data["links"]= explode('&nbsp;', $str_link);
	
		$this->load->view('pusat/manajemen',$data);
	}

}
