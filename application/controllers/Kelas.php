<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas extends CI_Controller {

	public function __construct(){
			parent::__construct();
			$this->load->model('model_m');
           
          }
	public function index()

	{
		$id_sekolah=$this->session->userdata('id_sekolah');
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		
		$data['guru']=$this->model_m->guru($id_sekolah);
		$this->load->view('sekolah/insertkelas',$data);
	}
 
 	public function data()

	{
		$id_sekolah=$this->session->userdata('id_sekolah');
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		$data['kls']=$this->model_m->kelas($id_sekolah);
		$this->load->view('sekolah/kelas',$data);
	}
		

	
    public function aksi_insert()

	{
		$config['upload_path'] = './uploads';
		$config['allowed_types'] = 'jpg|png|jpeg|pdf';		
			
		$this->upload->initialize($config);
		if($this->upload->do_upload('foto')){
			$data=$this->upload->data();			
			//$config['file_name'];
			$foto	= $data["file_name"];				
		}else{ $foto	=''; }
		$data1 = array(
							'nama_kelas'=>$this->input->post('f1'),
							'id_guru'=>$this->input->post('f2'),
							'id_sekolah'=>$this->input->post('sekolah'),
							
							//'foto'=>$foto,
						);
					$this->model_m->input_data('kelas',$data1);
					  redirect('Kelas/data');
	}
	public function ubah_kelas($id_kelas)

	{
		$where= array('id_kelas' => $id_kelas );
		$id_sekolah=$this->session->userdata('id_sekolah');
		$data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
		$data['guru']=$this->model_m->guru($id_sekolah);
		$data['kls']=$this->model_m->kelasjoin($id_kelas);
		$this->load->view('sekolah/ubahkelas',$data);
	}
	public function aksi_ubah()

	{
		$data1 = array(
							'nama_kelas'=>$this->input->post('f1'),
							'id_guru'=>$this->input->post('f2'),
							'id_sekolah'=>$this->input->post('sekolah'),
						);
					$where = array('id_kelas'=>$this->input->post('fid'));
					$this->model_m->update_data('kelas',$data1,$where);
					  redirect('Kelas/data');
	}

	
	
}
