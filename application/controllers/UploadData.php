<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UploadData extends CI_Controller {

	public function __construct(){
			parent::__construct();
			$this->load->model('model_m');
           $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		   if(!$this->session->userdata('username')){
          redirect('login');
       }
          }
	public function index()

	{
		 $id_sekolah=$this->session->userdata('id_sekolah');
		 $data['thn']=$this->model_m->selectsemua('tahun_ajaran');
   
    $data['sekolah']=$this->model_m->sklh($id_sekolah)->result();
    $data['data']= $this->model_m->terkirim();
		$this->load->view('sekolah/uploadfile',$data);
	}
 
 	public function aksi_insert()

	{
		$config['upload_path'] = './uploads';
		$config['allowed_types'] = 'xls|pdf|xlsx';		
			
		$this->upload->initialize($config);
		if($this->upload->do_upload('file')){
			$data=$this->upload->data();			
			//$config['file_name'];
			$file	= $data["file_name"];				
		}else{ $file	=''; }
		$data1 = array(
							'id_sekolah'=>$this->input->post('sekolah'),
							
							'id_tahunajaran'=>$this->input->post('thn'),
							'file'=>$file
							//'foto'=>$foto,
						);
					$this->model_m->input_data('rekap_bulan',$data1);
					 echo $this->upload->display_errors();
	}
	public function coba_lagi()

	{
		$file= $_FILES["file"];
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'pdf|xls';	
		$this->upload->initialize($config);	
			$this->load->library("upload",$config);
			if(!$this->upload->do_upload("file")){
				echo $this->upload->display_errors();
			}else{
				echo "success";
			}
		// // $this->upload->initialize($config);
		// if($this->upload->do_upload('file')){
		// 	$data=$this->upload->data();			
		// 	//$config['file_name'];
		// 	$file	= $data["file_name"];				
		// }else{ $file	=''; }
		// $data1 = array(
		// 					// 'id_sekolah'=>$this->input->post('sekolah'),
							
		// 					// 'id_tahunajaran'=>$this->input->post('thn'),
		// 					'file'=>$file
		// 					//'foto'=>$foto,
		// 				);
					// $this->model_m->input_data('coba',$data1);
					//   redirect('UploadData');
	}
	public function rekap()
	{
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));	
		 $fileName = $_FILES['file']['name'];
         
        $config['upload_path'] = './assets/'; //buat folder dengan nama assets di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 10000;
         
        $this->load->library('upload');
        $this->upload->initialize($config);
         
        if(! $this->upload->do_upload('file') )
        $this->upload->display_errors();
             
        $media = $this->upload->data('file');
     // $inputFileName = './assets/'.$media['file_name'];
		
		$inputFileName = './assets/rekap.xls';
		try {
			$inputFileType = IOFactory::identify($inputFileName);
			$objReader = IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFileName);
		} catch(Exception $e) {
			die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}

		$sheet = $objPHPExcel->getSheet(0);
		$highestRow = $sheet->getHighestRow();
		$highestColumn = $sheet->getHighestColumn();
			for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
					$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
													NULL,
													TRUE,
													FALSE);
					// $jenis_id = $this->jenis_id($rowData[0][10]);
							
								$data = array(					  
									//"id_siswa"=> $rowData[0][0],
                   					 "nama_siswa"=> $rowData[0][0],
                   					  "kelompok"=> $rowData[0][1],
				                    "bulan"=> $rowData[0][2],
				                   
				                    "tahun"=> $rowData[0][3],
				                    "program"=> $rowData[0][4],
				                     "minggu1"=> $rowData[0][5],
				                    "minggu2"=> $rowData[0][6],
				                    "minggu3"=> $rowData[0][7],
				                    "minggu4"=> $rowData[0][8],
				                    "minggu5"=> $rowData[0][9],
				                   
				                    'id_sekolah'=>$this->input->post('sekolah'),
							
							'id_tahunajaran'=>$this->input->post('thn'),
							//'kotakab'=>$this->input->post('kota'),
								);
								
							$insert = $this->db->insert("coba_rekap",$data);
						
						 }//unlink($inputFileName);
						//  redirect('UploadData/');
					
					//sesuaikan nama dengan nama tabel
					
		}
		public function uploadlg(){
  $fileName = $this->input->post('file', TRUE);

  $config['upload_path'] = './uploads/'; 
  $config['file_name'] = $fileName;
  $config['allowed_types'] = 'xls|xlsx|csv|ods|ots';
  $config['max_size'] = 10000;

  $this->load->library('upload', $config);
  $this->upload->initialize($config); 
  
  if (!$this->upload->do_upload('file')) {
   $error = array('error' => $this->upload->display_errors());
   $this->session->set_flashdata('msg','Ada kesalah dalam upload'); 
   // redirect('Welcome'); 
  } else {
   $media = $this->upload->data();
   $inputFileName = 'uploads/'.$media['file_name'];
   
   try {
    $inputFileType = IOFactory::identify($inputFileName);
    $objReader = IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
   } catch(Exception $e) {
    die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
   }

   $sheet = $objPHPExcel->getSheet(0);
   $highestRow = $sheet->getHighestRow();
   $highestColumn = $sheet->getHighestColumn();

   for ($row = 1; $row <= $highestRow; $row++){  
     $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
       NULL,
       TRUE,
       FALSE);
     $data = array(
    //"id_siswa"=> $rowData[0][0],
                   					
                   					"nama_siswa"=> $rowData[0][0],
                   					"kelompok"=> $rowData[0][1],
				                    "bulan"=> $rowData[0][2],
				                    "tahun"=> $rowData[0][3],
				                    "semester"=> $rowData[0][4],
				                    "program"=> $rowData[0][5],
				                    "indikator"=> $rowData[0][6],
				                    "nilai1"=> $rowData[0][7],
				                    "nilai2"=> $rowData[0][8],
				                    "nilai3"=> $rowData[0][9],
				                    "nilai4"=> $rowData[0][10],
				                    "nilai5"=> $rowData[0][11],
				                    "nilai6"=> $rowData[0][12],
				                   
				                    "id_sekolah"=>$this->input->post('sekolah'),
							
							"id_tahunajaran"=>$this->input->post('thn'),
    );
    $this->db->insert("coba_rekap",$data);
   } 
   $this->session->set_flashdata('msg','Berhasil upload ...!!'); 
   redirect('UploadData/');

  }  
 } 
	public function coba()
	{
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));	
		 $fileName = $_FILES['file']['name'];
         
        $config['upload_path'] = './assets'; //buat folder dengan nama assets di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 10000;
         
        $this->load->library('upload');
        $this->upload->initialize($config);
         
        if(! $this->upload->do_upload('file') ){

        $this->upload->display_errors();
        }
             
        var_dump($_FILES);
        $media = $this->upload->data('file');
     // $inputFileName = './assets/'.$media['file_name'];
		
		// $inputFileName = './assets/rekap.xlsx';
		// try {
		// 	$inputFileType = IOFactory::identify($inputFileName);
		// 	$objReader = IOFactory::createReader($inputFileType);
		// 	$objPHPExcel = $objReader->load($inputFileName);
		// } catch(Exception $e) {
		// 	die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		// }

		// $sheet = $objPHPExcel->getSheet(0);
		// $highestRow = $sheet->getHighestRow();
		// $highestColumn = $sheet->getHighestColumn();
		// 	for ($row = 1; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
		// 			$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
		// 											NULL,
		// 											TRUE,
		// 											FALSE);
		// 			// $jenis_id = $this->jenis_id($rowData[0][10]);
							
		// 						$data = array(					  
		// 							//"id_siswa"=> $rowData[0][0],
  //                  					//"id_siswa"=> $rowData[0][0],
  //                  					 "nama_siswa"=> $rowData[0][0],
  //                  					  "kelompok"=> $rowData[0][1],
		// 		                    "bulan"=> $rowData[0][2],
				                   
		// 		                    "tahun"=> $rowData[0][3],
		// 		                    "program"=> $rowData[0][4],
		// 		                     "minggu1"=> $rowData[0][5],
		// 		                    "minggu2"=> $rowData[0][6],
		// 		                    "minggu3"=> $rowData[0][7],
		// 		                    "minggu4"=> $rowData[0][8],
		// 		                    "minggu5"=> $rowData[0][9],
				                   
		// 		                    'id_sekolah'=>$this->input->post('sekolah'),
							
		// 					'id_tahunajaran'=>$this->input->post('thn'),
		// 					//'kotakab'=>$this->input->post('kota'),
		// 						);
								
		// 					$insert = $this->db->insert("coba_rekap",$data);
						
		// 				 } //unlink($inputFileName);
		// 				//  redirect('siswa/');
					
		// 			//sesuaikan nama dengan nama tabel
					
		}

	
	
	
}
