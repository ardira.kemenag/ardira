/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100116
 Source Host           : localhost:3306
 Source Schema         : acp_rapor

 Target Server Type    : MySQL
 Target Server Version : 100116
 File Encoding         : 65001

 Date: 01/05/2020 12:37:39
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for absensi
-- ----------------------------
DROP TABLE IF EXISTS `absensi`;
CREATE TABLE `absensi`  (
  `id_absen` int(11) NOT NULL AUTO_INCREMENT,
  `id_siswa` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_absen`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for coba_rekap
-- ----------------------------
DROP TABLE IF EXISTS `coba_rekap`;
CREATE TABLE `coba_rekap`  (
  `id_rekap` int(11) NOT NULL AUTO_INCREMENT,
  `id_sekolah` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_tahunajaran` int(11) NOT NULL,
  `nama_siswa` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `bulan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `semester` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `kelompok` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tahun` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `program` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `indikator` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `nilai1` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `nilai2` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `nilai3` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `nilai4` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `nilai5` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `nilai6` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `tanggal_upload` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id_rekap`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for guru
-- ----------------------------
DROP TABLE IF EXISTS `guru`;
CREATE TABLE `guru`  (
  `id_guru` int(11) NOT NULL AUTO_INCREMENT,
  `nama_guru` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  PRIMARY KEY (`id_guru`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of guru
-- ----------------------------
INSERT INTO `guru` VALUES (1, 'Holfiatun Muhammaroh', 1634);
INSERT INTO `guru` VALUES (2, 'Sugiyanti Rahmawati', 1634);
INSERT INTO `guru` VALUES (3, 'IIM IMNIAH', 12406);
INSERT INTO `guru` VALUES (4, 'SITI ULYANAH', 12406);
INSERT INTO `guru` VALUES (5, 'SITI JULAEHA', 12406);
INSERT INTO `guru` VALUES (6, 'MASRUROH', 12406);
INSERT INTO `guru` VALUES (7, 'MARWIYAH SAUWIH', 12419);
INSERT INTO `guru` VALUES (8, 'SRI WAHYUNI', 12419);
INSERT INTO `guru` VALUES (9, 'SUTIANI, S.Pd.', 4677);
INSERT INTO `guru` VALUES (10, 'SILFITRI PRATAMA, S.Pd', 4677);
INSERT INTO `guru` VALUES (11, 'ISMA MILLAH, S.Psi', 4677);
INSERT INTO `guru` VALUES (12, 'NURUL HIDAYATI, S.Pd.I', 2525);
INSERT INTO `guru` VALUES (13, 'UMI HANIK, S.Pd', 2525);
INSERT INTO `guru` VALUES (14, 'HENI', 2525);
INSERT INTO `guru` VALUES (15, 'NURSAIDAH, S.Pd', 2525);
INSERT INTO `guru` VALUES (16, 'NURUL HIDAYATI, S.Pd.I', 2525);
INSERT INTO `guru` VALUES (17, 'IVADA JAMIATUL HUSNIYAH, S.Pd.I', 7734);
INSERT INTO `guru` VALUES (18, 'KHODIJAH', 7734);
INSERT INTO `guru` VALUES (19, 'UMI MASRUROH, S.Pd.I', 7734);
INSERT INTO `guru` VALUES (20, 'KELAS A', 20038);

-- ----------------------------
-- Table structure for indikator
-- ----------------------------
DROP TABLE IF EXISTS `indikator`;
CREATE TABLE `indikator`  (
  `id_indikator` int(11) NOT NULL AUTO_INCREMENT,
  `id_program` int(11) NOT NULL,
  `kode_kd` varchar(23) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kode_indikator` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama_indikator` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  PRIMARY KEY (`id_indikator`) USING BTREE,
  INDEX `kode_kd`(`kode_kd`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 202 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of indikator
-- ----------------------------
INSERT INTO `indikator` VALUES (1, 1, '[\"1.1\"]', '1.1.1', 'Mempercayai adanya Tuhan melalui ciptaan Nya', 0);
INSERT INTO `indikator` VALUES (2, 1, '[\"1.1\"]', '1.1.2', 'Sikap mengagungkan Allah  melalui Do’a-do’a secara tertib (Adab doa)', 0);
INSERT INTO `indikator` VALUES (3, 1, '[\"1.1\"]', '1.1.3', 'Membiasakan dan mengungkapkan  Kalimat toyyibah', 0);
INSERT INTO `indikator` VALUES (4, 1, '[\"1.1\"]', '1.1.4', 'Menyebut beberapa asmaul husna', 0);
INSERT INTO `indikator` VALUES (5, 1, '[\"1.1\"]', '1.1.5', 'Menyebutkan beberapa nama malaikat', 0);
INSERT INTO `indikator` VALUES (6, 1, '[\"1.1\"]', '1.1.6', 'Menyebutkan tugas-tugas nabi', 0);
INSERT INTO `indikator` VALUES (7, 1, '[\"1.1\"]', '1.1.7', 'Menyebutkan beberapa nama Nabi dan Rosul ALLAH', 0);
INSERT INTO `indikator` VALUES (8, 1, '[\"1.1\"]', '1.1.8', 'Menyebutkan keluarga Nabi', 0);
INSERT INTO `indikator` VALUES (9, 1, '[\"1.1\"]', '1.1.9', 'Menyebutkan sahabat Nabi', 0);
INSERT INTO `indikator` VALUES (10, 1, '[\"1.1\"]', '1.1.10', 'Menyebutkan beberapa hadits Nabi', 0);
INSERT INTO `indikator` VALUES (11, 1, '[\"1.1\"]', '1.1.11', 'Menyebutkan nama kitab suci umat Islam', 0);
INSERT INTO `indikator` VALUES (12, 1, '[\"1.1\"]', '1.1.12', 'Menyebutkan hurup hijaiyah', 0);
INSERT INTO `indikator` VALUES (13, 1, '[\"1.1\"]', '1.1.13', 'Menghafalkan beberapa surat pendek dalam Al-qur’an', 0);
INSERT INTO `indikator` VALUES (14, 1, '[\"1.1\"]', '1.1.14', 'Mengucapkan syahadat Tauhid', 0);
INSERT INTO `indikator` VALUES (15, 1, '[\"1.1\"]', '1.1.15', 'Sikap Khusyu saat beribadah', 0);
INSERT INTO `indikator` VALUES (16, 1, '[\"1.1\"]', '1.1.16', 'Terbiasa menutup aurat', 0);
INSERT INTO `indikator` VALUES (17, 1, '[\"1.1\"]', '1.1.17', 'Membaca rangkaian huruf hijaiyah', 0);
INSERT INTO `indikator` VALUES (18, 1, '[\"1.1\"]', '1.1.18', 'Mengucapkan syahadat tauhid', 0);
INSERT INTO `indikator` VALUES (19, 1, '[\"1.1\"]', '1.1.19', 'Menyebutkan arti syahadat tauhid', 0);
INSERT INTO `indikator` VALUES (20, 1, '[\"1.1\"]', '1.1.20', 'Menyebutkan nama-nama shalat lima waktu', 0);
INSERT INTO `indikator` VALUES (21, 1, '[\"1.1\"]', '1.1.21', 'Menyebutkan jumlah rakaat shalat lima waktu', 0);
INSERT INTO `indikator` VALUES (22, 1, '[\"1.1\"]', '1.1.22', 'Menirukan gerakan shalat', 0);
INSERT INTO `indikator` VALUES (23, 1, '[\"1.1\"]', '1.1.23', 'Melafalkan bacaan shalat', 0);
INSERT INTO `indikator` VALUES (24, 1, '[\"1.1\"]', '1.1.24', 'Menirukan gerakan wudhu', 0);
INSERT INTO `indikator` VALUES (25, 1, '[\"1.1\"]', '1.1.25', 'Menyebutkan arti zakat', 0);
INSERT INTO `indikator` VALUES (26, 1, '[\"1.1\"]', '1.1.26', 'Menyebutkan arti shodaqoh', 0);
INSERT INTO `indikator` VALUES (27, 1, '[\"1.1\"]', '1.1.27', 'Mempraktekan zakat', 0);
INSERT INTO `indikator` VALUES (28, 1, '[\"1.1\"]', '1.1.28', 'Mempraktekan shodaqoh', 0);
INSERT INTO `indikator` VALUES (29, 1, '[\"1.1\"]', '1.1.29', 'Menyebutkan arti shaum', 0);
INSERT INTO `indikator` VALUES (30, 1, '[\"1.1\"]', '1.1.30', 'Menyebutkan arti sholat ied', 0);
INSERT INTO `indikator` VALUES (31, 1, '[\"1.1\"]', '1.1.31', 'Menyebutkan tatacara haji secara sederhana', 0);
INSERT INTO `indikator` VALUES (32, 1, '[\"1.1\"]', '1.1.32', 'Menyebut tokoh ibadah qurban', 0);
INSERT INTO `indikator` VALUES (33, 1, '[\"1.1\"]', '1.1.33', 'Menghapal beberapa doa harian', 0);
INSERT INTO `indikator` VALUES (34, 1, '[\"1.1\"]', '1.1.34', 'Menyebutkan beberapa kalimah toyyibah', 0);
INSERT INTO `indikator` VALUES (35, 1, '[\"1.2\"]', '1.2.1', 'Sikap sopan santun saat berbicara ', 0);
INSERT INTO `indikator` VALUES (36, 1, '[\"1.2\"]', '1.2.2', 'Sikap meminta maaf dan memaafkan orang lain', 0);
INSERT INTO `indikator` VALUES (37, 1, '[\"1.2\"]', '1.2.3', 'Berpakaian yang baik dan menutup aurat', 0);
INSERT INTO `indikator` VALUES (38, 1, '[\"2.13\"]', '1.3.1', 'Terbiasa tidak sombong', 0);
INSERT INTO `indikator` VALUES (39, 1, '[\"2.13\"]', '1.3.2', 'Terbiasa menghargai kepemilikan orang lain', 0);
INSERT INTO `indikator` VALUES (40, 1, '[\"2.13\"]', '1.3.3', 'Terbiasa mengembalikan benda yang bukan hak nya', 0);
INSERT INTO `indikator` VALUES (41, 1, '[\"3.1\",\"4.1\"]', '1.5.1', 'Mengucapkan doa-doa pendek melakukan ibadah sehari-hari,berdoa sebelum dan sesudah kegiatan', 0);
INSERT INTO `indikator` VALUES (42, 1, '[\"3.1\",\"4.1\"]', '1.5.2', 'Berprilaku sesuai dengan ajaran agama,tidak sombong', 0);
INSERT INTO `indikator` VALUES (43, 1, '[\"3.1\",\"4.1\"]', '1.5.3', 'Menyebutkan hari-hari besar agama', 0);
INSERT INTO `indikator` VALUES (44, 1, '[\"3.1\",\"4.1\"]', '1.5.4', 'Menyebutkan tempat ibadah', 0);
INSERT INTO `indikator` VALUES (45, 1, '[\"3.1\",\"4.1\"]', '1.5.5', 'Menceritakan kembali nabi nabi utusan Allah', 0);
INSERT INTO `indikator` VALUES (46, 1, '[\"3.2\",\"4.2\"]', '1.7.1', 'Berprilaku sopan dan peduli ', 0);
INSERT INTO `indikator` VALUES (47, 1, '[\"3.2\",\"4.2\"]', '1.7.2', 'Mau menolong orang tua,guru,dan teman', 0);
INSERT INTO `indikator` VALUES (48, 1, '[\"3.2\",\"4.2\"]', '1.7.3', 'Mau menerima tugas dengan ikhlas', 0);
INSERT INTO `indikator` VALUES (49, 1, '[\"3.2\",\"4.2\"]', '1.7.4', 'Terbiasa melakukan kegiatan sendiri', 0);
INSERT INTO `indikator` VALUES (50, 1, '[\"3.2\",\"4.2\"]', '1.7.5', 'Terbiasa mengikuti tata tertib dan aturan sekolah', 0);
INSERT INTO `indikator` VALUES (51, 1, '[\"3.2\",\"4.2\"]', '1.7.6', 'Tepat waktu saat berangkat dan pulang sekolah', 0);
INSERT INTO `indikator` VALUES (52, 1, '[\"3.2\",\"4.2\"]', '1.7.7', 'Terbiasa berhenti bermain pada waktunya', 0);
INSERT INTO `indikator` VALUES (53, 1, '[\"3.2\",\"4.2\"]', '1.7.8', 'Rapi dalam bertindak dan bekerja', 0);
INSERT INTO `indikator` VALUES (54, 1, '[\"3.2\",\"4.2\"]', '1.7.9', 'Tanggung jawab atas tugas yang diberikan', 0);
INSERT INTO `indikator` VALUES (55, 1, '[\"3.2\",\"4.2\"]', '1.7.10', 'Terbiasa mengembalikan mainan ke tempatnya', 0);
INSERT INTO `indikator` VALUES (56, 1, '[\"3.2\",\"4.2\"]', '1.7.11', 'Dapat membedakan milik sendiri dan sekolah', 0);
INSERT INTO `indikator` VALUES (57, 1, '[\"3.2\",\"4.2\"]', '1.7.12', 'Terbiasa mengambil makanan secukupnya dan makan sendiri', 0);
INSERT INTO `indikator` VALUES (58, 1, '[\"3.2\",\"4.2\"]', '1.7.13', 'Berani karena benar dan mempunyai rasa ingin tahu yang besar', 0);
INSERT INTO `indikator` VALUES (59, 1, '[\"3.2\",\"4.2\"]', '1.7.14', 'Terbiasa mengerjakan keperluannya sendiri', 0);
INSERT INTO `indikator` VALUES (60, 1, '[\"3.2\",\"4.2\"]', '1.7.15', 'Sabar menunggu giliran', 0);
INSERT INTO `indikator` VALUES (61, 1, '[\"3.2\",\"4.2\"]', '1.7.16', 'Dapat dibujuk', 0);
INSERT INTO `indikator` VALUES (62, 1, '[\"3.2\",\"4.2\"]', '1.7.17', 'Tidak cengeng', 0);
INSERT INTO `indikator` VALUES (63, 1, '[\"3.2\",\"4.2\"]', '1.7.18', 'Mengendalikan emosi dengan cara yang wajar', 0);
INSERT INTO `indikator` VALUES (64, 1, '[\"3.2\",\"4.2\"]', '1.7.19', 'Berani tampil didepan umum', 0);
INSERT INTO `indikator` VALUES (65, 1, '[\"3.2\",\"4.2\"]', '1.7.20', 'Berani mempertahankan pendapatnya', 0);
INSERT INTO `indikator` VALUES (66, 2, '[\"2.1\"]', '2.1.1', 'Terbiasa melakukan kegiatan-kegiatan sendiri', 0);
INSERT INTO `indikator` VALUES (67, 2, '[\"2.1\"]', '2.1.2', 'Terbiasa makan makanan bergizi dan seimbang', 0);
INSERT INTO `indikator` VALUES (68, 2, '[\"2.1\"]', '2.1.3', 'Terbiasa memelihara kebersihan lingkungan', 0);
INSERT INTO `indikator` VALUES (69, 2, '[\"3.3\",\"4.3\"]', '2.3.1', 'Melakukan berbagai gerakan terkordinasi secara terkontrol,seimbang dan lincah.', 0);
INSERT INTO `indikator` VALUES (70, 2, '[\"3.3\",\"4.3\"]', '2.3.2', 'Melakukan gerakan mata, tangan,kaki, kepala secara terkordinasi dalam menirukan berbagai gerakan yang teratur ( misal: senam dan tarian).', 0);
INSERT INTO `indikator` VALUES (71, 2, '[\"3.3\",\"4.3\"]', '2.3.3', 'Melakukan permainan fisik dengan aturan', 0);
INSERT INTO `indikator` VALUES (72, 2, '[\"3.3\",\"4.3\"]', '2.3.4', 'Terampil menggunakan tangan kanan dan kiri dalam berbagai aktivitas (misal: mengancingkannya baju, menali sepatu, menggambar, menempel, menggunting pola, meniru bentuk, menggunakan alat makan )', 0);
INSERT INTO `indikator` VALUES (73, 2, '[\"3.4\",\"4.4\"]', '2.5.1', 'Melakukan kebiasaan hidup bersih dan sehat (misal: mandi 2x sehari, memakai baju bersih, membuang sampah pada tempatnya, menutup hidung dan mulut ketika batuk dan bersin, membersihkan dan membereskan dan membersihkan tempat bermain)', 0);
INSERT INTO `indikator` VALUES (74, 2, '[\"3.4\",\"4.4\"]', '2.5.2', 'Mampu melindungi diri dari percobaan kekerasan, termasuk kekerasan seksual dan bullying (misal : dengan berteriak dan atau berlari)', 0);
INSERT INTO `indikator` VALUES (75, 2, '[\"3.4\",\"4.4\"]', '2.5.3', 'Mampu menjaga keamanan diri dari benda-benda berbahaya (misal: Listrik, pisau, pembasmi serangga, kendaraan di jalan raya)', 0);
INSERT INTO `indikator` VALUES (76, 2, '[\"3.4\",\"4.4\"]', '2.5.4', 'Menggunakan toilet dengan benar tanpa bantuan', 0);
INSERT INTO `indikator` VALUES (77, 2, '[\"3.4\",\"4.4\"]', '2.5.5', 'Mengenal kebiasaan buruk bagi kesehatan (makan permen, jajan sembarang tempat)', 0);
INSERT INTO `indikator` VALUES (78, 3, '[\"2.2\"]', '3.1.1', 'Terbiasa menunjukkan aktivitas yang bersifat eksploratif dan menyelidik (seperti: apa yang terjadi ketika air ditumpahkan)', 0);
INSERT INTO `indikator` VALUES (79, 3, '[\"2.2\"]', '3.1.2', 'Terbiasa aktif bertanya', 0);
INSERT INTO `indikator` VALUES (80, 3, '[\"2.2\"]', '3.1.3', 'Terbiasa mencoba atau melakukan melakukan sesuatu  untuk mendapatkan jawaban ', 0);
INSERT INTO `indikator` VALUES (81, 3, '[\"2.3\"]', '3.2.1', 'Kreatif dalam menyelesaikan masalah ( ide, gagasan, diluar kebiasaan atau cara yang tidak biasa )', 0);
INSERT INTO `indikator` VALUES (82, 3, '[\"2.3\"]', '3.2.2', 'Menunjukan inisiatif dalam memilih permainan ( seperti:’ayo kita bermain pura-pura seperti burung”)', 0);
INSERT INTO `indikator` VALUES (83, 3, '[\"2.3\"]', '3.2.3', 'Senang menerapkan pengetahuan atau pengalam dalam situasi atau sesuatu yang baru.', 0);
INSERT INTO `indikator` VALUES (84, 3, '[\"3.5\",\"4.5\"]', '3.4.1', 'Mampu memecahkan sendiri masalah sederhana yang dihadapi', 0);
INSERT INTO `indikator` VALUES (85, 3, '[\"3.5\",\"4.5\"]', '3.4.2', 'Menyelesaikan tugas meskipun menghadapi kesulitan', 0);
INSERT INTO `indikator` VALUES (86, 3, '[\"3.5\",\"4.5\"]', '3.4.3', 'Menyusun perencanaan kegiatan yang akan dilakukan', 0);
INSERT INTO `indikator` VALUES (87, 3, '[\"3.5\",\"4.5\"]', '3.4.4', 'Menyesuaikan diri dengan cuaca dan kondisi alam', 0);
INSERT INTO `indikator` VALUES (88, 3, '[\"3.5\",\"4.5\"]', '3.4.5', 'Menyelesaikan tugas meskipun menghadapi kesulitan', 0);
INSERT INTO `indikator` VALUES (89, 3, '[\"3.6\",\"4.6\"]', '3.6.1', 'Mengenal benda dengan mengelompokkan berbagai benda dilingkungannya berdasarkan ukuran,sifat, suara, tekstur, fungsi dan ciri-ciri lainnya', 0);
INSERT INTO `indikator` VALUES (90, 3, '[\"3.6\",\"4.6\"]', '3.6.2', 'Mengenal benda dengan menghubungkan satu benda dengan benda yang lain', 0);
INSERT INTO `indikator` VALUES (91, 3, '[\"3.6\",\"4.6\"]', '3.6.3', 'Menghubungkan atau menjodohkan nama benda dengan tulisan sederhana melalui berbagai aktifitas', 0);
INSERT INTO `indikator` VALUES (92, 3, '[\"3.6\",\"4.6\"]', '3.6.4', 'Mengenal konsep besar kecil, banyak sedikit, panjang pendek, berat ringan, tinggi rendah, dengan mengukur menggunakan alat ukur tidak baku', 0);
INSERT INTO `indikator` VALUES (93, 3, '[\"3.6\",\"4.6\"]', '3.6.5', 'Membuat pola ABCD -ABCD', 0);
INSERT INTO `indikator` VALUES (94, 3, '[\"3.6\",\"4.6\"]', '3.6.6', 'Mampu mengurutkan lima seriasi atau lebih berdasarkan warna, bentuk, ukuran atau jumlah,', 0);
INSERT INTO `indikator` VALUES (95, 3, '[\"3.6\",\"4.6\"]', '3.6.7', 'Mengenal perbedaan berdasarkan ukuran : “ lebih dari”.,”kurang dari’., dan “paling/ter” ', 0);
INSERT INTO `indikator` VALUES (96, 3, '[\"3.6\",\"4.6\"]', '3.6.8', 'Mengklasifikasikan benda berdasarkan tiga pariabel warna, bentuk dan ukuran', 0);
INSERT INTO `indikator` VALUES (97, 3, '[\"3.6\",\"4.6\"]', '3.6.9', 'Menyebutkan lambang bilangan 1-10', 0);
INSERT INTO `indikator` VALUES (98, 3, '[\"3.6\",\"4.6\"]', '3.6.10', 'Menggunakan lambang bilangan untuk menghitung', 0);
INSERT INTO `indikator` VALUES (99, 3, '[\"3.6\",\"4.6\"]', '3.6.11', 'Mencocokkan bilangan dengan lambang bilangan', 0);
INSERT INTO `indikator` VALUES (100, 3, '[\"3.7\",\"4.7\"]', '3.8.1', 'Menyebutkan nama anggota keluarga danteman serta ciri-ciri husus mereka secara lebih rinci ( warna kulit,warna rambut, jenis rambut dll)', 0);
INSERT INTO `indikator` VALUES (101, 3, '[\"3.7\",\"4.7\"]', '3.8.2', 'Menjelaskan lingkungan sekitarnya secara sederhana', 0);
INSERT INTO `indikator` VALUES (102, 3, '[\"3.7\",\"4.7\"]', '3.8.3', 'Menyebutkan arah ketempat yang sering dikunjungi dan alat transportasi yang digunakan', 0);
INSERT INTO `indikator` VALUES (103, 3, '[\"3.7\",\"4.7\"]', '3.8.4', 'Menyebutkan peran-peran dan pekerjaan termasuk didalamnya perlengkapan/ atribut dan tugas-tugas yang dilakukan dalam pekerjaan tersebut', 0);
INSERT INTO `indikator` VALUES (104, 3, '[\"3.7\",\"4.7\"]', '3.8.5', 'Membuat dan mengikuti aturan ', 0);
INSERT INTO `indikator` VALUES (105, 3, '[\"3.8\",\"4.8\"]', '3.10.1', 'Menceritakan peristiwa-peristiwa alam dengan melakukan percobaan sederhana ', 0);
INSERT INTO `indikator` VALUES (106, 3, '[\"3.8\",\"4.8\"]', '3.10.2', 'Mengungkapkan hasil karya yang dibuatnya secara lengkap/utuh yang berhubungan dengan benda-benda yang ada dilingkungan alam', 0);
INSERT INTO `indikator` VALUES (107, 3, '[\"3.8\",\"4.8\"]', '3.10.3', 'Menceritakan perkembang biakan makhluk hidup', 0);
INSERT INTO `indikator` VALUES (108, 3, '[\"3.8\",\"4.8\"]', '3.10.4', 'Mengenal sebab akibat tentang lingkungannya ( angin bertiup menyebabkan daun bergerak, air dapat menyebabkan sesuatu basah)', 0);
INSERT INTO `indikator` VALUES (109, 3, '[\"3.9\",\"4.9\"]', '3.12.1', 'Melakukan kegiatan dengan menggunakan alat tekhnologi sederhana sesuai pungsinya secara aman dan bertanggung jawab', 0);
INSERT INTO `indikator` VALUES (110, 3, '[\"3.9\",\"4.9\"]', '3.12.2', 'Membuat alat-alat tekhnologi sederhana (misalnya: baling-baling)', 0);
INSERT INTO `indikator` VALUES (111, 3, '[\"3.9\",\"4.9\"]', '3.12.3', 'Melakukan proses kerja sesuai dengan prosedurnya (misal: membuat teh dimulai dari menyediakan air panas)', 0);
INSERT INTO `indikator` VALUES (112, 4, '[\"2.14\"]', '4.1.1', 'Terbiasa ramah menyapa siapapun', 0);
INSERT INTO `indikator` VALUES (113, 4, '[\"2.14\"]', '4.1.2', 'Terbiasa berkata lembut dan santun', 0);
INSERT INTO `indikator` VALUES (114, 4, '[\"3.10\",\"4.10\"]', '4.3.1', 'Menceritakan kembali apa yang didengar dengan kosa kata yang lebih banyak', 0);
INSERT INTO `indikator` VALUES (115, 4, '[\"3.10\",\"4.10\"]', '4.3.2', 'Melaksanakan perintah yang lebik komplek sesuai dengan aturan yang sesuai dengan aturan yang disampaikan', 0);
INSERT INTO `indikator` VALUES (116, 4, '[\"3.10\",\"4.10\"]', '4.3.3', 'Mengulang kalimat yang lebih komplek', 0);
INSERT INTO `indikator` VALUES (117, 4, '[\"3.10\",\"4.10\"]', '4.3.4', 'Memahami aturan dalam suatu permainan', 0);
INSERT INTO `indikator` VALUES (118, 4, '[\"3.11\",\"4.11\"]', '4.5.1', 'Mengungkapkan keinginan, perasaan, dan pendapat dengan kalimat sederhana dalam berkomunikasi dengan anak atau orang dewasa', 0);
INSERT INTO `indikator` VALUES (119, 4, '[\"3.11\",\"4.11\"]', '4.5.2', 'Menunjukkan perilaku senang membaca buku terhadap buku-buku yang dikenali', 0);
INSERT INTO `indikator` VALUES (120, 4, '[\"3.11\",\"4.11\"]', '4.5.3', 'Mengungkapkan perasaan, ide dengan pilihan kata yang sesuai ketika berkomunikasi', 0);
INSERT INTO `indikator` VALUES (121, 4, '[\"3.11\",\"4.11\"]', '4.5.4', 'Menceritakan kembali isi cerita cesara sederhana', 0);
INSERT INTO `indikator` VALUES (122, 4, '[\"3.11\",\"4.11\"]', '4.5.5', 'Menjawab pertanyaan yang lebik kompleks', 0);
INSERT INTO `indikator` VALUES (123, 4, '[\"3.11\",\"4.11\"]', '4.5.6', 'Menyebutkan kelompok gambar yang memiliki bunyi yang sama', 0);
INSERT INTO `indikator` VALUES (124, 4, '[\"3.11\",\"4.11\"]', '4.5.7', 'Menyusun kalimat sederhana dalam struktur lengkap', 0);
INSERT INTO `indikator` VALUES (125, 4, '[\"3.12\",\"4.12\"]', '4.7.1', 'Menunjukkan bentuk-bentuk simbol ( pola Menulis )', 0);
INSERT INTO `indikator` VALUES (126, 4, '[\"3.12\",\"4.12\"]', '4.7.2', 'Menyebutkan lambang-lambang huruf yang dikenal', 0);
INSERT INTO `indikator` VALUES (127, 4, '[\"3.12\",\"4.12\"]', '4.7.3', 'Menulis hurup- hurup dari namanya sendiri', 0);
INSERT INTO `indikator` VALUES (128, 4, '[\"3.12\",\"4.12\"]', '4.7.4', 'Mengenal suara hurup awal dari nama-nama benda disekitarnya', 0);
INSERT INTO `indikator` VALUES (129, 4, '[\"3.12\",\"4.12\"]', '4.7.5', 'Menyebutkan kelompok gambar yang memiliki bunyi/hurup awal yang sama', 0);
INSERT INTO `indikator` VALUES (130, 4, '[\"3.12\",\"4.12\"]', '4.7.6', 'Mengenal berbagai macam lambang  huruf fokal dan konsonan', 0);
INSERT INTO `indikator` VALUES (131, 4, '[\"3.12\",\"4.12\"]', '4.7.7', 'Memahami hubungan antara bunyi dan bentuk huruf', 0);
INSERT INTO `indikator` VALUES (132, 4, '[\"3.12\",\"4.12\"]', '4.7.8', 'Membaca nama sendiri ', 0);
INSERT INTO `indikator` VALUES (133, 4, '[\"3.12\",\"4.12\"]', '4.7.9', 'Membuat gambar dengan beberapa coretan /tulisan yang berbentuk hurup/ kata', 0);
INSERT INTO `indikator` VALUES (134, 4, '[\"3.12\",\"4.12\"]', '4.7.10', 'Menyebutkan angka bila diperlihatkan lambang bilangannya ( mengucapkan bunyi lambang bilangan)', 0);
INSERT INTO `indikator` VALUES (135, 4, '[\"3.12\",\"4.12\"]', '4.7.11', 'Senang dan menghargai bacaan', 0);
INSERT INTO `indikator` VALUES (136, 4, '[\"3.12\",\"4.12\"]', '4.7.12', 'Memahami arti kata dalam cerita', 0);
INSERT INTO `indikator` VALUES (137, 4, '[\"3.12\",\"4.12\"]', '4.7.13', 'Memiliki perbendahaan kata ', 0);
INSERT INTO `indikator` VALUES (138, 4, '[\"3.12\",\"4.12\"]', '4.7.14', 'Mengenal simbol-simbol untuk persiapan membaca,menulis dan berhitung', 0);
INSERT INTO `indikator` VALUES (139, 5, '[\"2.5\"]', '5.1.1', 'Terbiasa menyapa guru saat penyambutan', 0);
INSERT INTO `indikator` VALUES (140, 5, '[\"2.5\"]', '5.1.2', 'Berani tambil didepan teman, guru, orang tua dan lingkungan.dan sosial lainnya', 0);
INSERT INTO `indikator` VALUES (141, 5, '[\"2.5\"]', '5.1.3', 'Berani mengemukakan pendapat', 0);
INSERT INTO `indikator` VALUES (142, 5, '[\"2.5\"]', '5.1.4', 'Berani menyampaikan keinginan', 0);
INSERT INTO `indikator` VALUES (143, 5, '[\"2.5\"]', '5.1.5', 'Berkomunikasi dengan orang yang belum dikenal sebelumnya dengan pengawasan guru', 0);
INSERT INTO `indikator` VALUES (144, 5, '[\"2.5\"]', '5.1.6', 'Bangga menunjukkan hasil karya', 0);
INSERT INTO `indikator` VALUES (145, 5, '[\"2.5\"]', '5.1.7', 'Senang ikut serta dalam kegiatan bersama', 0);
INSERT INTO `indikator` VALUES (146, 5, '[\"2.5\"]', '5.1.8', 'Tidak berpengaruh penilaan oranng tentang dirinya', 0);
INSERT INTO `indikator` VALUES (147, 5, '[\"2.6\"]', '5.2.1', 'Tau akan haknya', 0);
INSERT INTO `indikator` VALUES (148, 5, '[\"2.6\"]', '5.2.2', 'Mentaati aturan kelas ( kegiatan, aturan)', 0);
INSERT INTO `indikator` VALUES (149, 5, '[\"2.6\"]', '5.2.3', 'Mengatur diri sendiri', 0);
INSERT INTO `indikator` VALUES (150, 5, '[\"2.7\"]', '5.3.1', 'Kesediaan diri untuk menahan diri', 0);
INSERT INTO `indikator` VALUES (151, 5, '[\"2.7\"]', '5.3.2', 'Bersikap tenang tidak lekas marah dan dapat menunda keinginan', 0);
INSERT INTO `indikator` VALUES (152, 5, '[\"2.7\"]', '5.3.3', 'Sikap mau menunggu giliran , mau mendengarkan ketika orang lain bicara', 0);
INSERT INTO `indikator` VALUES (153, 5, '[\"2.7\"]', '5.3.4', 'Tidak menangis saat berpisah dengan orang tuanya', 0);
INSERT INTO `indikator` VALUES (154, 5, '[\"2.7\"]', '5.3.5', 'Tidak mudah mengeluh ', 0);
INSERT INTO `indikator` VALUES (155, 5, '[\"2.7\"]', '5.3.6', 'Tidak tergesa-gesa', 0);
INSERT INTO `indikator` VALUES (156, 5, '[\"2.7\"]', '5.3.7', 'Selalu menyesaikan gagasan-gagasannya hingga tuntas', 0);
INSERT INTO `indikator` VALUES (157, 5, '[\"2.7\"]', '5.3.8', 'Berusaha tidak  menyakiti atau membalas kekerasan', 0);
INSERT INTO `indikator` VALUES (158, 5, '[\"2.8\"]', '5.4.1', 'Terbiasa tidak bergantung pada orang lain', 0);
INSERT INTO `indikator` VALUES (159, 5, '[\"2.8\"]', '5.4.2', 'Terbiasa mengambil keputusan secara mandiri', 0);
INSERT INTO `indikator` VALUES (160, 5, '[\"2.8\"]', '5.4.3', 'Merencanakan, memilih, memiliki inisiatif untuk belajar atau melakukan sesuatu tanpa harus dibantu atau dibantu seperlunya', 0);
INSERT INTO `indikator` VALUES (161, 5, '[\"2.9\"]', '5.5.1', 'Mengetahui perasaan temannya dan meresponn secara wajar', 0);
INSERT INTO `indikator` VALUES (162, 5, '[\"2.9\"]', '5.5.2', 'Berbagi dengan orang lain', 0);
INSERT INTO `indikator` VALUES (163, 5, '[\"2.9\"]', '5.5.3', 'Menghargai/hak/pendapat/karya orang lain', 0);
INSERT INTO `indikator` VALUES (164, 5, '[\"2.9\"]', '5.5.4', 'Terbiasa mengindahkan dan memperhatikan kondisi teman', 0);
INSERT INTO `indikator` VALUES (165, 5, '[\"2.9\"]', '5.5.5', 'Mau menemani teman melakukan kegiatan bersama', 0);
INSERT INTO `indikator` VALUES (166, 5, '[\"2.9\"]', '5.5.6', 'Senang menawarkan bantuan pada teman atau gurupeka untuk membantu orang lain yang membutuhkan', 0);
INSERT INTO `indikator` VALUES (167, 5, '[\"2.9\"]', '5.5.7', 'Mampu menenangkan diri dan temannya dalam berbagai situasai,', 0);
INSERT INTO `indikator` VALUES (168, 5, '[\"2.9\"]', '5.5.8', 'Senang mengajak temannya untuk berkomunikasi,beraksi positif kepada semua temannya', 0);
INSERT INTO `indikator` VALUES (169, 5, '[\"2.10\"]', '5.6.1', 'Bermain dengan teman sebaya', 0);
INSERT INTO `indikator` VALUES (170, 5, '[\"2.10\"]', '5.6.2', 'Menerima perbedaan teman dengan dirinya', 0);
INSERT INTO `indikator` VALUES (171, 5, '[\"2.10\"]', '5.6.3', 'Menghargai karya teman', 0);
INSERT INTO `indikator` VALUES (172, 5, '[\"2.10\"]', '5.6.4', 'Tidak ingin menang sendiri', 0);
INSERT INTO `indikator` VALUES (173, 5, '[\"2.10\"]', '5.6.5', 'Menghargai pendapat pendapat teman dan mendengarkan dengan sabar pendapat teman', 0);
INSERT INTO `indikator` VALUES (174, 5, '[\"2.10\"]', '5.6.6', 'Senang berteman dengan semuanya', 0);
INSERT INTO `indikator` VALUES (175, 5, '[\"2.11\"]', '5.7.1', 'Memperlihatkan diri untuk menyesuaikan dengan situasi', 0);
INSERT INTO `indikator` VALUES (176, 5, '[\"2.11\"]', '5.7.2', 'Memperlihatkan kehati-hatian kepada orang yang belum dikenal ( menumbuhkan kepercayaan kepada pada orang dewasa yang tepat)', 0);
INSERT INTO `indikator` VALUES (177, 5, '[\"2.11\"]', '5.7.3', 'Bersikap kooperatif dengan teman', 0);
INSERT INTO `indikator` VALUES (178, 5, '[\"2.11\"]', '5.7.4', 'Menggunakan cara yang diterima secara sosial dalam menyelesaikan masalah ( menggunakan pikiran untuk menyelesaikan masalah)', 0);
INSERT INTO `indikator` VALUES (179, 5, '[\"2.11\"]', '5.7.5', 'Tetap tenang saat ditempat yang baru dengan situasai baru misal:saat bertemu, berada dipusat perbelanjaan, atau saat bertemu dengan guru baru,', 0);
INSERT INTO `indikator` VALUES (180, 5, '[\"2.12\"]', '5.8.1', 'Bertanggung jawab atas perilaku untuk kebaikan diri sendiri', 0);
INSERT INTO `indikator` VALUES (181, 5, '[\"2.12\"]', '5.8.2', 'Bersedia untuk menerima konsekwensi atau menanggung akibat atas tindakan yang diperbuat baik secara sengaja maupuntidak sengaja', 0);
INSERT INTO `indikator` VALUES (182, 5, '[\"2.12\"]', '5.8.3', 'Mau mengakui kesalahan dengan meminta maaf', 0);
INSERT INTO `indikator` VALUES (183, 5, '[\"2.12\"]', '5.8.4', 'Merapihkan/ membereskan mainan pada tempat semula', 0);
INSERT INTO `indikator` VALUES (184, 5, '[\"2.12\"]', '5.8.5', 'Mengerjakan sesuatu hingga tuntas', 0);
INSERT INTO `indikator` VALUES (185, 5, '[\"2.12\"]', '5.8.6', 'Senang menjalankan kegiatan yang jadi tugasnya (misalnya piket sebagai pemimpin harus membantu menyiapkan alat makan )', 0);
INSERT INTO `indikator` VALUES (186, 5, '[\"3.14\",\"4.14\"]', '5.10.1', 'mengenal perasaan sendiri dan orang lain', 0);
INSERT INTO `indikator` VALUES (187, 5, '[\"3.14\",\"4.14\"]', '5.10.2', 'mengelolanya secara wajar ( mengendalikan diri secara wajar)', 0);
INSERT INTO `indikator` VALUES (188, 5, '[\"3.14\",\"4.14\"]', '5.10.3', 'Berprilaku yang membuat orang lain nyaman', 0);
INSERT INTO `indikator` VALUES (189, 5, '[\"3.14\",\"4.14\"]', '5.10.4', 'Mengekspresikan emosi yang sesuai dengan kondisi yang ada ( senang-sedih-antusias dsb )', 0);
INSERT INTO `indikator` VALUES (190, 5, '[\"3.14\",\"4.14\"]', '5.12.1', 'Memilih kegiatan/ benda yang paling sesuai dengan yang dibutuhkan dari beberapa pilihan yang ada', 0);
INSERT INTO `indikator` VALUES (191, 5, '[\"3.14\",\"4.14\"]', '5.12.2', 'Mengungkapkan yang dirasakan ( lapar ingin makan, kedinginan, memerlikan baju hangat, perlu payung agar tidak keehujanan, kepanasan, sakit perut perlu berobat)', 0);
INSERT INTO `indikator` VALUES (192, 5, '[\"3.14\",\"4.14\"]', '5.12.3', 'Menggunakan sesuatu sesuai kebutuhan', 0);
INSERT INTO `indikator` VALUES (193, 6, '[\"2.4\"]', '6.1.1', 'Menghargai keindahan diri sendiri, karya sendiri atau orang lain, alam dan lingkungan sekitar', 0);
INSERT INTO `indikator` VALUES (194, 6, '[\"2.4\"]', '6.1.2', 'Menjaga kerapihan diri', 0);
INSERT INTO `indikator` VALUES (195, 6, '[\"2.4\"]', '6.1.3', 'Bertindak /berbuat yang mencerminkan sikap estetis', 0);
INSERT INTO `indikator` VALUES (196, 6, '[\"2.4\"]', '6.1.4', 'Merawat kerapihan, kebersihan, dan keutuhan benda mainan atau milik pribadinya', 0);
INSERT INTO `indikator` VALUES (197, 6, '[\"3.15\",\"4.15\"]', '6.3.1', 'Membuat karya seni sesuai kreativitasnya misal seni musik, visual, gerak, tari yang dihasilkannya dengan menggunakan alat yang sesuai', 0);
INSERT INTO `indikator` VALUES (198, 6, '[\"3.15\",\"4.15\"]', '6.3.2', 'Menampilkan hasil karya seni baik dalam bentuk gambar', 0);
INSERT INTO `indikator` VALUES (199, 6, '[\"3.15\",\"4.15\"]', '6.3.3', 'Menghargai hasil karya baik dalam bentuk gambar', 0);
INSERT INTO `indikator` VALUES (200, 1, '[\"1.1\"]', '1.1.15 ', 'Memperdalam keberadaan-NYA', 1);
INSERT INTO `indikator` VALUES (201, 1, '[\"1.2\"]', '1.2.17', 'Bertukar pendapat dengan teman', 1);

-- ----------------------------
-- Table structure for kd
-- ----------------------------
DROP TABLE IF EXISTS `kd`;
CREATE TABLE `kd`  (
  `id_kd` int(11) NOT NULL AUTO_INCREMENT,
  `id_KI` int(11) NOT NULL,
  `kode_kd` varchar(23) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama_kd` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_kd`) USING BTREE,
  INDEX `id_KI`(`id_KI`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 47 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of kd
-- ----------------------------
INSERT INTO `kd` VALUES (1, 1, '1.1', 'Mengenal Tuhan Melalui Ciptaannya');
INSERT INTO `kd` VALUES (2, 1, '1.2', 'Menghargai diri sendiri, orang lain, dan lingkungan sekitar sebagai rasa syukur kepada Tuhan');
INSERT INTO `kd` VALUES (3, 2, '2.1', ' Memiliki perilaku yang mencerminkan hidup sehat');
INSERT INTO `kd` VALUES (4, 2, '2.2', 'Memiliki perilaku yang mencerminkan sikap ingin tahu');
INSERT INTO `kd` VALUES (5, 2, '2.3', 'Memiliki perilaku yang mencerminkan sikap kreatif');
INSERT INTO `kd` VALUES (6, 2, '2.4', 'Memiliki perilaku yang mencerminkan sikap Estetis');
INSERT INTO `kd` VALUES (7, 2, '2.5', ' Memiliki perilaku yang mencerminkan sikap percaya diri');
INSERT INTO `kd` VALUES (8, 2, '2.6', 'Memiliki perilaku yang mencerminkan sikap taat terhadap aturan sehari-hari untuk melatih kedisiplinan');
INSERT INTO `kd` VALUES (9, 2, '2.7', 'Memiliki perilaku yang mencerminkan sikap sabar (mau menunggu giliran, mau mendengar ketika orang lain berbicara) untuk melatih kedisiplinan');
INSERT INTO `kd` VALUES (10, 2, '2.8', 'Memiliki perilaku yang mencerminkan kemandirian');
INSERT INTO `kd` VALUES (11, 2, '2.9', 'Memiliki perilaku yang mencerminkan sikap peduli dan mau membantu jika diminta bantuannya');
INSERT INTO `kd` VALUES (12, 2, '2.10', ' Memiliki perilaku yang mencerminkan sikap menghargai dan toleran kepada orang lain');
INSERT INTO `kd` VALUES (13, 2, '2.11', ' Memiliki perilaku yang dapat menyesuaikan diri');
INSERT INTO `kd` VALUES (14, 2, '2.12', 'Memiliki perilaku yang mencerminkan sikap tanggungjawab');
INSERT INTO `kd` VALUES (15, 2, '2.13', 'Memiliki perilaku yang mencerminkan sikap jujur');
INSERT INTO `kd` VALUES (16, 2, '2.14', '  Memiliki perilaku yang mencerminkan sikap rendah hati dan santun kepada orang tua, pendidik, dan teman');
INSERT INTO `kd` VALUES (17, 3, '3.1', ' Mengenal kegiatan beribadah sehari-hari');
INSERT INTO `kd` VALUES (18, 3, '3.2', '  Mengetahui cara hidup sehat');
INSERT INTO `kd` VALUES (19, 3, '3.3', '   Mengenal perilaku baik sebagai cerminan akhlak mulia');
INSERT INTO `kd` VALUES (20, 3, '3.4', '   Mengenal anggota tubuh, fungsi, dan gerakannya untuk pengembangan motorik kasar dan motorik halus');
INSERT INTO `kd` VALUES (21, 3, '3.5', '   Mengetahui cara memecahkan masalah sehari-hari dan berperilaku kreatif');
INSERT INTO `kd` VALUES (22, 3, '3.6', '  Mengenal benda-benda disekitarnya (nama, warna, bentuk, ukuran, pola, sifat, suara, tekstur, fungsi, dan ciri-ciri lainnya)');
INSERT INTO `kd` VALUES (23, 3, '3.7', ' Mengenal lingkungan sosial (keluarga, teman, tempat tinggal, tempat ibadah, budaya, transportasi)');
INSERT INTO `kd` VALUES (24, 3, '3.8', '  Mengenal lingkungan alam (hewan, tanaman, cuaca, tanah, air, batu-batuan, dll)');
INSERT INTO `kd` VALUES (25, 3, '3.9', ' Mengenal teknologi sederhana (peralatan rumah tangga, peralatan bermain, peralatan pertukangan, dll)');
INSERT INTO `kd` VALUES (26, 3, '3.10', '  Memahami bahasa reseptif (menyimak dan membaca)');
INSERT INTO `kd` VALUES (27, 3, '3.11', ' Memahami bahasa ekspresif (mengungkapkan bahasa secara verbaldan non verbal)');
INSERT INTO `kd` VALUES (28, 3, '3.12', '  Mengenal keaksaraan awal melalui bermain');
INSERT INTO `kd` VALUES (29, 3, '3.13', '  Mengenal emosi diri dan orang lain');
INSERT INTO `kd` VALUES (30, 3, '3.14', '  Mengenali kebutuhan, keinginan, dan minat diri');
INSERT INTO `kd` VALUES (31, 3, '3.15', ' Mengenal berbagai karya dan aktivitas seni');
INSERT INTO `kd` VALUES (32, 4, '4.1', ' Melakukan kegiatan beribadah sehari-hari dengan tuntunan orang dewasa');
INSERT INTO `kd` VALUES (33, 4, '4.2', ' Menunjukkan perilaku santun sebagai cerminan akhlak mulia');
INSERT INTO `kd` VALUES (34, 4, '4.3', '   Menggunakan anggota tubuh untuk pengembangan motorik kasar dan halus');
INSERT INTO `kd` VALUES (35, 4, '4.4', '   Mampu menolong diri sendiri untuk hidup sehat');
INSERT INTO `kd` VALUES (36, 4, '4.5', '  Menyelesaikan masalah sehari-hari secara kreatif');
INSERT INTO `kd` VALUES (37, 4, '4.6', '   Menyampaikan tentang apa dan bagaimana benda-benda di sekitar yang dikenalnya (nama, warna, bentuk, ukuran, pola, sifat, suara, tekstur, fungsi, dan ciriciri lainnya) melalui berbagai hasil karya');
INSERT INTO `kd` VALUES (38, 4, '4.7', '   Menyajikan berbagai karya yang berhubungan dengan lingkungan sosial (keluarga, teman, tempat tinggal, tempat ibadah, budaya, transportasi) dalam bentuk gambar, bercerita, bernyanyi, dan gerak tubuh');
INSERT INTO `kd` VALUES (39, 4, '4.8', 'Menyajikan berbagai karya yang berhubungan dengan lingkungan alam (hewan, tanaman, cuaca, tanah, air, batubatuan, dll) dalam bentuk gambar, bercerita, bernyanyi, dan gerak tubuh');
INSERT INTO `kd` VALUES (40, 4, '4.9', '  Menggunakan teknologi sederhana untuk menyelesaikan tugas dan kegiatannya (peralatan rumah tangga, peralatan bermain, peralatan pertukangan, dll)');
INSERT INTO `kd` VALUES (41, 4, '4.10', ' Menunjukkan kemampuan berbahasa reseptif (menyimak dan membaca)');
INSERT INTO `kd` VALUES (42, 4, '4.11', '  Menunjukkan kemampuan berbahasa ekspresif (mengungkapkan bahasa secara verbal dan non verbal)');
INSERT INTO `kd` VALUES (43, 4, '4.12', ' Menunjukkan kemampuan keaksaraan awal dalam berbagai bentuk karya');
INSERT INTO `kd` VALUES (44, 4, '4.13', ' Menunjukkan reaksi emosi diri secara wajar');
INSERT INTO `kd` VALUES (45, 4, '4.14', ' Mengungkapkan kebutuhan, keinginan dan minat diri dengan cara yang tepat');
INSERT INTO `kd` VALUES (46, 4, '4.15', ' Menunjukkan karya dan aktivitas seni dengan menggunakan berbagai media');

-- ----------------------------
-- Table structure for kelas
-- ----------------------------
DROP TABLE IF EXISTS `kelas`;
CREATE TABLE `kelas`  (
  `id_kelas` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kelas` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_guru` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  PRIMARY KEY (`id_kelas`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of kelas
-- ----------------------------
INSERT INTO `kelas` VALUES (1, 'A', 3, 12406);
INSERT INTO `kelas` VALUES (2, 'B1', 4, 12406);
INSERT INTO `kelas` VALUES (3, 'B2', 5, 12406);
INSERT INTO `kelas` VALUES (4, 'B3', 6, 12406);
INSERT INTO `kelas` VALUES (5, 'B', 7, 12419);
INSERT INTO `kelas` VALUES (6, 'A', 8, 12419);
INSERT INTO `kelas` VALUES (7, 'B', 10, 4677);
INSERT INTO `kelas` VALUES (8, 'A', 11, 4677);
INSERT INTO `kelas` VALUES (9, 'A', 14, 2525);
INSERT INTO `kelas` VALUES (10, 'B', 13, 2525);

-- ----------------------------
-- Table structure for ki
-- ----------------------------
DROP TABLE IF EXISTS `ki`;
CREATE TABLE `ki`  (
  `id_KI` int(11) NOT NULL AUTO_INCREMENT,
  `nama_KI` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_KI`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ki
-- ----------------------------
INSERT INTO `ki` VALUES (1, ' Menerima ajaran agama yang dianutnya');
INSERT INTO `ki` VALUES (2, 'Memiliki perilaku hidup sehat, rasa ingin tahu, kreatif dan estetis, percaya diri, disiplin, mandiri, peduli,</n>\r\nmampu menghargai dan toleran kepada orang lain, mampu menyesuaikan diri, jujur, rendah hati dan santun dalam berinteraksi dengan keluarga, pendidik, dan teman');
INSERT INTO `ki` VALUES (3, 'KI-3. Mengenali diri, keluarga, teman,pendidik, lingkungan sekitar, agama, teknologi, seni, dan budaya di rumah,tempat bermain dan RA dengan cara: mengamati dengan indera(melihat, mendengar, menghidu, merasa, meraba);menanya;mengumpulkan informasi;menalar;\ndan mengomunikasikan melalui kegiatanbermain');
INSERT INTO `ki` VALUES (4, 'Menunjukkan yang diketahui,dirasakan, dibutuhkan, dan dipikirkan melalui bahasa, musik, gerakan, dan karya secara produktif dan kreatif, serta mencerminkan perilaku anak berakhlak mulia');

-- ----------------------------
-- Table structure for kota_kab
-- ----------------------------
DROP TABLE IF EXISTS `kota_kab`;
CREATE TABLE `kota_kab`  (
  `id_kotaKab` int(11) NOT NULL,
  `id_provinsi` int(11) NOT NULL,
  `nama_kota` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_kotaKab`) USING BTREE,
  INDEX `id_provinsi`(`id_provinsi`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of kota_kab
-- ----------------------------
INSERT INTO `kota_kab` VALUES (1101, 11, 'Kab. Simeulue');
INSERT INTO `kota_kab` VALUES (1102, 11, 'Kab. Aceh Singkil');
INSERT INTO `kota_kab` VALUES (1103, 11, 'Kab. Aceh Selatan');
INSERT INTO `kota_kab` VALUES (1104, 11, 'Kab. Aceh Tenggara');
INSERT INTO `kota_kab` VALUES (1105, 11, 'Kab. Aceh Timur');
INSERT INTO `kota_kab` VALUES (1106, 11, 'Kab. Aceh Tengah');
INSERT INTO `kota_kab` VALUES (1107, 11, 'Kab. Aceh Barat');
INSERT INTO `kota_kab` VALUES (1108, 11, 'Kab. Aceh Besar');
INSERT INTO `kota_kab` VALUES (1109, 11, 'Kab. Pidie');
INSERT INTO `kota_kab` VALUES (1110, 11, 'Kab. Bireuen');
INSERT INTO `kota_kab` VALUES (1111, 11, 'Kab. Aceh Utara');
INSERT INTO `kota_kab` VALUES (1112, 11, 'Kab. Aceh Barat Daya');
INSERT INTO `kota_kab` VALUES (1113, 11, 'Kab. Gayo Lues');
INSERT INTO `kota_kab` VALUES (1114, 11, 'Kab. Aceh Tamiang');
INSERT INTO `kota_kab` VALUES (1115, 11, 'Kab. Nagan Raya');
INSERT INTO `kota_kab` VALUES (1116, 11, 'Kab. Aceh Jaya');
INSERT INTO `kota_kab` VALUES (1117, 11, 'Kab. Bener Meriah');
INSERT INTO `kota_kab` VALUES (1118, 11, 'Kab. Pidie Jaya');
INSERT INTO `kota_kab` VALUES (1171, 11, 'Kota Banda Aceh');
INSERT INTO `kota_kab` VALUES (1172, 11, 'Kota Sabang');
INSERT INTO `kota_kab` VALUES (1173, 11, 'Kota Langsa');
INSERT INTO `kota_kab` VALUES (1174, 11, 'Kota Lhokseumawe');
INSERT INTO `kota_kab` VALUES (1175, 11, 'Kota Subulussalam');
INSERT INTO `kota_kab` VALUES (1201, 12, 'Kab. Nias');
INSERT INTO `kota_kab` VALUES (1202, 12, 'Kab. Mandailing Natal');
INSERT INTO `kota_kab` VALUES (1203, 12, 'Kab. Tapanuli Selatan');
INSERT INTO `kota_kab` VALUES (1204, 12, 'Kab. Tapanuli Tengah');
INSERT INTO `kota_kab` VALUES (1205, 12, 'Kab. Tapanuli Utara');
INSERT INTO `kota_kab` VALUES (1206, 12, 'Kab. Toba Samosir');
INSERT INTO `kota_kab` VALUES (1207, 12, 'Kab. Labuhan Batu');
INSERT INTO `kota_kab` VALUES (1208, 12, 'Kab. Asahan');
INSERT INTO `kota_kab` VALUES (1209, 12, 'Kab. Simalungun');
INSERT INTO `kota_kab` VALUES (1210, 12, 'Kab. Dairi');
INSERT INTO `kota_kab` VALUES (1211, 12, 'Kab. Karo');
INSERT INTO `kota_kab` VALUES (1212, 12, 'Kab. Deli Serdang');
INSERT INTO `kota_kab` VALUES (1213, 12, 'Kab. Langkat');
INSERT INTO `kota_kab` VALUES (1214, 12, 'Kab. Nias Selatan');
INSERT INTO `kota_kab` VALUES (1215, 12, 'Kab. Humbang Hasundutan');
INSERT INTO `kota_kab` VALUES (1216, 12, 'Kab. Pakpak Bharat');
INSERT INTO `kota_kab` VALUES (1217, 12, 'Kab. Samosir');
INSERT INTO `kota_kab` VALUES (1218, 12, 'Kab. Serdang Bedagai');
INSERT INTO `kota_kab` VALUES (1219, 12, 'Kab. Batu Bara');
INSERT INTO `kota_kab` VALUES (1220, 12, 'Kab. Padang Lawas Utara');
INSERT INTO `kota_kab` VALUES (1221, 12, 'Kab. Padang Lawas');
INSERT INTO `kota_kab` VALUES (1222, 12, 'Kab. Labuhan Batu Selatan');
INSERT INTO `kota_kab` VALUES (1223, 12, 'Kab. Labuhan Batu Utara');
INSERT INTO `kota_kab` VALUES (1224, 12, 'Kab. Nias Utara');
INSERT INTO `kota_kab` VALUES (1225, 12, 'Kab. Nias Barat');
INSERT INTO `kota_kab` VALUES (1271, 12, 'Kota Sibolga');
INSERT INTO `kota_kab` VALUES (1272, 12, 'Kota Tanjung Balai');
INSERT INTO `kota_kab` VALUES (1273, 12, 'Kota Pematang Siantar');
INSERT INTO `kota_kab` VALUES (1274, 12, 'Kota Tebing Tinggi');
INSERT INTO `kota_kab` VALUES (1275, 12, 'Kota Medan');
INSERT INTO `kota_kab` VALUES (1276, 12, 'Kota Binjai');
INSERT INTO `kota_kab` VALUES (1277, 12, 'Kota Padangsidimpuan');
INSERT INTO `kota_kab` VALUES (1278, 12, 'Kota Gunungsitoli');
INSERT INTO `kota_kab` VALUES (1301, 13, 'Kab. Kepulauan Mentawai');
INSERT INTO `kota_kab` VALUES (1302, 13, 'Kab. Pesisir Selatan');
INSERT INTO `kota_kab` VALUES (1303, 13, 'Kab. Solok');
INSERT INTO `kota_kab` VALUES (1304, 13, 'Kab. Sijunjung');
INSERT INTO `kota_kab` VALUES (1305, 13, 'Kab. Tanah Datar');
INSERT INTO `kota_kab` VALUES (1306, 13, 'Kab. Padang Pariaman');
INSERT INTO `kota_kab` VALUES (1307, 13, 'Kab. Agam');
INSERT INTO `kota_kab` VALUES (1308, 13, 'Kab. Lima Puluh Kota');
INSERT INTO `kota_kab` VALUES (1309, 13, 'Kab. Pasaman');
INSERT INTO `kota_kab` VALUES (1310, 13, 'Kab. Solok Selatan');
INSERT INTO `kota_kab` VALUES (1311, 13, 'Kab. Dharmasraya');
INSERT INTO `kota_kab` VALUES (1312, 13, 'Kab. Pasaman Barat');
INSERT INTO `kota_kab` VALUES (1371, 13, 'Kota Padang');
INSERT INTO `kota_kab` VALUES (1372, 13, 'Kota Solok');
INSERT INTO `kota_kab` VALUES (1373, 13, 'Kota Sawah Lunto');
INSERT INTO `kota_kab` VALUES (1374, 13, 'Kota Padang Panjang');
INSERT INTO `kota_kab` VALUES (1375, 13, 'Kota Bukittinggi');
INSERT INTO `kota_kab` VALUES (1376, 13, 'Kota Payakumbuh');
INSERT INTO `kota_kab` VALUES (1377, 13, 'Kota Pariaman');
INSERT INTO `kota_kab` VALUES (1401, 14, 'Kab. Kuantan Singingi');
INSERT INTO `kota_kab` VALUES (1402, 14, 'Kab. Indragiri Hulu');
INSERT INTO `kota_kab` VALUES (1403, 14, 'Kab. Indragiri Hilir');
INSERT INTO `kota_kab` VALUES (1404, 14, 'Kab. Pelalawan');
INSERT INTO `kota_kab` VALUES (1405, 14, 'Kab. S I A K');
INSERT INTO `kota_kab` VALUES (1406, 14, 'Kab. Kampar');
INSERT INTO `kota_kab` VALUES (1407, 14, 'Kab. Rokan Hulu');
INSERT INTO `kota_kab` VALUES (1408, 14, 'Kab. Bengkalis');
INSERT INTO `kota_kab` VALUES (1409, 14, 'Kab. Rokan Hilir');
INSERT INTO `kota_kab` VALUES (1410, 14, 'Kab. Kepulauan Meranti');
INSERT INTO `kota_kab` VALUES (1471, 14, 'Kota Pekanbaru');
INSERT INTO `kota_kab` VALUES (1473, 14, 'Kota D U M A I');
INSERT INTO `kota_kab` VALUES (1501, 15, 'Kab. Kerinci');
INSERT INTO `kota_kab` VALUES (1502, 15, 'Kab. Merangin');
INSERT INTO `kota_kab` VALUES (1503, 15, 'Kab. Sarolangun');
INSERT INTO `kota_kab` VALUES (1504, 15, 'Kab. Batang Hari');
INSERT INTO `kota_kab` VALUES (1505, 15, 'Kab. Muaro Jambi');
INSERT INTO `kota_kab` VALUES (1506, 15, 'Kab. Tanjung Jabung Timur');
INSERT INTO `kota_kab` VALUES (1507, 15, 'Kab. Tanjung Jabung Barat');
INSERT INTO `kota_kab` VALUES (1508, 15, 'Kab. Tebo');
INSERT INTO `kota_kab` VALUES (1509, 15, 'Kab. Bungo');
INSERT INTO `kota_kab` VALUES (1571, 15, 'Kota Jambi');
INSERT INTO `kota_kab` VALUES (1572, 15, 'Kota Sungai Penuh');
INSERT INTO `kota_kab` VALUES (1601, 16, 'Kab. Ogan Komering Ulu');
INSERT INTO `kota_kab` VALUES (1602, 16, 'Kab. Ogan Komering Ilir');
INSERT INTO `kota_kab` VALUES (1603, 16, 'Kab. Muara Enim');
INSERT INTO `kota_kab` VALUES (1604, 16, 'Kab. Lahat');
INSERT INTO `kota_kab` VALUES (1605, 16, 'Kab. Musi Rawas');
INSERT INTO `kota_kab` VALUES (1606, 16, 'Kab. Musi Banyuasin');
INSERT INTO `kota_kab` VALUES (1607, 16, 'Kab. Banyu Asin');
INSERT INTO `kota_kab` VALUES (1608, 16, 'Kab. Ogan Komering Ulu Selatan');
INSERT INTO `kota_kab` VALUES (1609, 16, 'Kab. Ogan Komering Ulu Timur');
INSERT INTO `kota_kab` VALUES (1610, 16, 'Kab. Ogan Ilir');
INSERT INTO `kota_kab` VALUES (1611, 16, 'Kab. Empat Lawang');
INSERT INTO `kota_kab` VALUES (1671, 16, 'Kota Palembang');
INSERT INTO `kota_kab` VALUES (1672, 16, 'Kota Prabumulih');
INSERT INTO `kota_kab` VALUES (1673, 16, 'Kota Pagar Alam');
INSERT INTO `kota_kab` VALUES (1674, 16, 'Kota Lubuklinggau');
INSERT INTO `kota_kab` VALUES (1701, 17, 'Kab. Bengkulu Selatan');
INSERT INTO `kota_kab` VALUES (1702, 17, 'Kab. Rejang Lebong');
INSERT INTO `kota_kab` VALUES (1703, 17, 'Kab. Bengkulu Utara');
INSERT INTO `kota_kab` VALUES (1704, 17, 'Kab. Kaur');
INSERT INTO `kota_kab` VALUES (1705, 17, 'Kab. Seluma');
INSERT INTO `kota_kab` VALUES (1706, 17, 'Kab. Mukomuko');
INSERT INTO `kota_kab` VALUES (1707, 17, 'Kab. Lebong');
INSERT INTO `kota_kab` VALUES (1708, 17, 'Kab. Kepahiang');
INSERT INTO `kota_kab` VALUES (1709, 17, 'Kab. Bengkulu Tengah');
INSERT INTO `kota_kab` VALUES (1771, 17, 'Kota Bengkulu');
INSERT INTO `kota_kab` VALUES (1801, 18, 'Kab. Lampung Barat');
INSERT INTO `kota_kab` VALUES (1802, 18, 'Kab. Tanggamus');
INSERT INTO `kota_kab` VALUES (1803, 18, 'Kab. Lampung Selatan');
INSERT INTO `kota_kab` VALUES (1804, 18, 'Kab. Lampung Timur');
INSERT INTO `kota_kab` VALUES (1805, 18, 'Kab. Lampung Tengah');
INSERT INTO `kota_kab` VALUES (1806, 18, 'Kab. Lampung Utara');
INSERT INTO `kota_kab` VALUES (1807, 18, 'Kab. Way Kanan');
INSERT INTO `kota_kab` VALUES (1808, 18, 'Kab. Tulangbawang');
INSERT INTO `kota_kab` VALUES (1809, 18, 'Kab. Pesawaran');
INSERT INTO `kota_kab` VALUES (1810, 18, 'Kab. Pringsewu');
INSERT INTO `kota_kab` VALUES (1811, 18, 'Kab. Mesuji');
INSERT INTO `kota_kab` VALUES (1812, 18, 'Kab. Tulang Bawang Barat');
INSERT INTO `kota_kab` VALUES (1813, 18, 'Kab. Pesisir Barat');
INSERT INTO `kota_kab` VALUES (1871, 18, 'Kota Bandar Lampung');
INSERT INTO `kota_kab` VALUES (1872, 18, 'Kota Metro');
INSERT INTO `kota_kab` VALUES (1901, 19, 'Kab. Bangka');
INSERT INTO `kota_kab` VALUES (1902, 19, 'Kab. Belitung');
INSERT INTO `kota_kab` VALUES (1903, 19, 'Kab. Bangka Barat');
INSERT INTO `kota_kab` VALUES (1904, 19, 'Kab. Bangka Tengah');
INSERT INTO `kota_kab` VALUES (1905, 19, 'Kab. Bangka Selatan');
INSERT INTO `kota_kab` VALUES (1906, 19, 'Kab. Belitung Timur');
INSERT INTO `kota_kab` VALUES (1971, 19, 'Kota Pangkal Pinang');
INSERT INTO `kota_kab` VALUES (2101, 21, 'Kab. Karimun');
INSERT INTO `kota_kab` VALUES (2102, 21, 'Kab. Bintan');
INSERT INTO `kota_kab` VALUES (2103, 21, 'Kab. Natuna');
INSERT INTO `kota_kab` VALUES (2104, 21, 'Kab. Lingga');
INSERT INTO `kota_kab` VALUES (2105, 21, 'Kab. Kepulauan Anambas');
INSERT INTO `kota_kab` VALUES (2171, 21, 'Kota B A T A M');
INSERT INTO `kota_kab` VALUES (2172, 21, 'Kota Tanjung Pinang');
INSERT INTO `kota_kab` VALUES (3101, 31, 'Kab. Kepulauan Seribu');
INSERT INTO `kota_kab` VALUES (3171, 31, 'Kota Jakarta Selatan');
INSERT INTO `kota_kab` VALUES (3172, 31, 'Kota Jakarta Timur');
INSERT INTO `kota_kab` VALUES (3173, 31, 'Kota Jakarta Pusat');
INSERT INTO `kota_kab` VALUES (3174, 31, 'Kota Jakarta Barat');
INSERT INTO `kota_kab` VALUES (3175, 31, 'Kota Jakarta Utara');
INSERT INTO `kota_kab` VALUES (3201, 32, 'Kab. Bogor');
INSERT INTO `kota_kab` VALUES (3202, 32, 'Kab. Sukabumi');
INSERT INTO `kota_kab` VALUES (3203, 32, 'Kab. Cianjur');
INSERT INTO `kota_kab` VALUES (3204, 32, 'Kab. Bandung');
INSERT INTO `kota_kab` VALUES (3205, 32, 'Kab. Garut');
INSERT INTO `kota_kab` VALUES (3206, 32, 'Kab. Tasikmalaya');
INSERT INTO `kota_kab` VALUES (3207, 32, 'Kab. Ciamis');
INSERT INTO `kota_kab` VALUES (3208, 32, 'Kab. Kuningan');
INSERT INTO `kota_kab` VALUES (3209, 32, 'Kab. Cirebon');
INSERT INTO `kota_kab` VALUES (3210, 32, 'Kab. Majalengka');
INSERT INTO `kota_kab` VALUES (3211, 32, 'Kab. Sumedang');
INSERT INTO `kota_kab` VALUES (3212, 32, 'Kab. Indramayu');
INSERT INTO `kota_kab` VALUES (3213, 32, 'Kab. Subang');
INSERT INTO `kota_kab` VALUES (3214, 32, 'Kab. Purwakarta');
INSERT INTO `kota_kab` VALUES (3215, 32, 'Kab. Karawang');
INSERT INTO `kota_kab` VALUES (3216, 32, 'Kab. Bekasi');
INSERT INTO `kota_kab` VALUES (3217, 32, 'Kab. Bandung Barat');
INSERT INTO `kota_kab` VALUES (3218, 32, 'Kab. Pangandaran');
INSERT INTO `kota_kab` VALUES (3271, 32, 'Kota Bogor');
INSERT INTO `kota_kab` VALUES (3272, 32, 'Kota Sukabumi');
INSERT INTO `kota_kab` VALUES (3273, 32, 'Kota Bandung');
INSERT INTO `kota_kab` VALUES (3274, 32, 'Kota Cirebon');
INSERT INTO `kota_kab` VALUES (3275, 32, 'Kota Bekasi');
INSERT INTO `kota_kab` VALUES (3276, 32, 'Kota Depok');
INSERT INTO `kota_kab` VALUES (3277, 32, 'Kota Cimahi');
INSERT INTO `kota_kab` VALUES (3278, 32, 'Kota Tasikmalaya');
INSERT INTO `kota_kab` VALUES (3279, 32, 'Kota Banjar');
INSERT INTO `kota_kab` VALUES (3301, 33, 'Kab. Cilacap');
INSERT INTO `kota_kab` VALUES (3302, 33, 'Kab. Banyumas');
INSERT INTO `kota_kab` VALUES (3303, 33, 'Kab. Purbalingga');
INSERT INTO `kota_kab` VALUES (3304, 33, 'Kab. Banjarnegara');
INSERT INTO `kota_kab` VALUES (3305, 33, 'Kab. Kebumen');
INSERT INTO `kota_kab` VALUES (3306, 33, 'Kab. Purworejo');
INSERT INTO `kota_kab` VALUES (3307, 33, 'Kab. Wonosobo');
INSERT INTO `kota_kab` VALUES (3308, 33, 'Kab. Magelang');
INSERT INTO `kota_kab` VALUES (3309, 33, 'Kab. Boyolali');
INSERT INTO `kota_kab` VALUES (3310, 33, 'Kab. Klaten');
INSERT INTO `kota_kab` VALUES (3311, 33, 'Kab. Sukoharjo');
INSERT INTO `kota_kab` VALUES (3312, 33, 'Kab. Wonogiri');
INSERT INTO `kota_kab` VALUES (3313, 33, 'Kab. Karanganyar');
INSERT INTO `kota_kab` VALUES (3314, 33, 'Kab. Sragen');
INSERT INTO `kota_kab` VALUES (3315, 33, 'Kab. Grobogan');
INSERT INTO `kota_kab` VALUES (3316, 33, 'Kab. Blora');
INSERT INTO `kota_kab` VALUES (3317, 33, 'Kab. Rembang');
INSERT INTO `kota_kab` VALUES (3318, 33, 'Kab. Pati');
INSERT INTO `kota_kab` VALUES (3319, 33, 'Kab. Kudus');
INSERT INTO `kota_kab` VALUES (3320, 33, 'Kab. Jepara');
INSERT INTO `kota_kab` VALUES (3321, 33, 'Kab. Demak');
INSERT INTO `kota_kab` VALUES (3322, 33, 'Kab. Semarang');
INSERT INTO `kota_kab` VALUES (3323, 33, 'Kab. Temanggung');
INSERT INTO `kota_kab` VALUES (3324, 33, 'Kab. Kendal');
INSERT INTO `kota_kab` VALUES (3325, 33, 'Kab. Batang');
INSERT INTO `kota_kab` VALUES (3326, 33, 'Kab. Pekalongan');
INSERT INTO `kota_kab` VALUES (3327, 33, 'Kab. Pemalang');
INSERT INTO `kota_kab` VALUES (3328, 33, 'Kab. Tegal');
INSERT INTO `kota_kab` VALUES (3329, 33, 'Kab. Brebes');
INSERT INTO `kota_kab` VALUES (3371, 33, 'Kota Magelang');
INSERT INTO `kota_kab` VALUES (3372, 33, 'Kota Surakarta');
INSERT INTO `kota_kab` VALUES (3373, 33, 'Kota Salatiga');
INSERT INTO `kota_kab` VALUES (3374, 33, 'Kota Semarang');
INSERT INTO `kota_kab` VALUES (3375, 33, 'Kota Pekalongan');
INSERT INTO `kota_kab` VALUES (3376, 33, 'Kota Tegal');
INSERT INTO `kota_kab` VALUES (3401, 34, 'Kab. Kulon Progo');
INSERT INTO `kota_kab` VALUES (3402, 34, 'Kab. Bantul');
INSERT INTO `kota_kab` VALUES (3403, 34, 'Kab. Gunung Kidul');
INSERT INTO `kota_kab` VALUES (3404, 34, 'Kab. Sleman');
INSERT INTO `kota_kab` VALUES (3471, 34, 'Kota Yogyakarta');
INSERT INTO `kota_kab` VALUES (3501, 35, 'Kab. Pacitan');
INSERT INTO `kota_kab` VALUES (3502, 35, 'Kab. Ponorogo');
INSERT INTO `kota_kab` VALUES (3503, 35, 'Kab. Trenggalek');
INSERT INTO `kota_kab` VALUES (3504, 35, 'Kab. Tulungagung');
INSERT INTO `kota_kab` VALUES (3505, 35, 'Kab. Blitar');
INSERT INTO `kota_kab` VALUES (3506, 35, 'Kab. Kediri');
INSERT INTO `kota_kab` VALUES (3507, 35, 'Kab. Malang');
INSERT INTO `kota_kab` VALUES (3508, 35, 'Kab. Lumajang');
INSERT INTO `kota_kab` VALUES (3509, 35, 'Kab. Jember');
INSERT INTO `kota_kab` VALUES (3510, 35, 'Kab. Banyuwangi');
INSERT INTO `kota_kab` VALUES (3511, 35, 'Kab. Bondowoso');
INSERT INTO `kota_kab` VALUES (3512, 35, 'Kab. Situbondo');
INSERT INTO `kota_kab` VALUES (3513, 35, 'Kab. Probolinggo');
INSERT INTO `kota_kab` VALUES (3514, 35, 'Kab. Pasuruan');
INSERT INTO `kota_kab` VALUES (3515, 35, 'Kab. Sidoarjo');
INSERT INTO `kota_kab` VALUES (3516, 35, 'Kab. Mojokerto');
INSERT INTO `kota_kab` VALUES (3517, 35, 'Kab. Jombang');
INSERT INTO `kota_kab` VALUES (3518, 35, 'Kab. Nganjuk');
INSERT INTO `kota_kab` VALUES (3519, 35, 'Kab. Madiun');
INSERT INTO `kota_kab` VALUES (3520, 35, 'Kab. Magetan');
INSERT INTO `kota_kab` VALUES (3521, 35, 'Kab. Ngawi');
INSERT INTO `kota_kab` VALUES (3522, 35, 'Kab. Bojonegoro');
INSERT INTO `kota_kab` VALUES (3523, 35, 'Kab. Tuban');
INSERT INTO `kota_kab` VALUES (3524, 35, 'Kab. Lamongan');
INSERT INTO `kota_kab` VALUES (3525, 35, 'Kab. Gresik');
INSERT INTO `kota_kab` VALUES (3526, 35, 'Kab. Bangkalan');
INSERT INTO `kota_kab` VALUES (3527, 35, 'Kab. Sampang');
INSERT INTO `kota_kab` VALUES (3528, 35, 'Kab. Pamekasan');
INSERT INTO `kota_kab` VALUES (3529, 35, 'Kab. Sumenep');
INSERT INTO `kota_kab` VALUES (3571, 35, 'Kota Kediri');
INSERT INTO `kota_kab` VALUES (3572, 35, 'Kota Blitar');
INSERT INTO `kota_kab` VALUES (3573, 35, 'Kota Malang');
INSERT INTO `kota_kab` VALUES (3574, 35, 'Kota Probolinggo');
INSERT INTO `kota_kab` VALUES (3575, 35, 'Kota Pasuruan');
INSERT INTO `kota_kab` VALUES (3576, 35, 'Kota Mojokerto');
INSERT INTO `kota_kab` VALUES (3577, 35, 'Kota Madiun');
INSERT INTO `kota_kab` VALUES (3578, 35, 'Kota Surabaya');
INSERT INTO `kota_kab` VALUES (3579, 35, 'Kota Batu');
INSERT INTO `kota_kab` VALUES (3601, 36, 'Kab. Pandeglang');
INSERT INTO `kota_kab` VALUES (3602, 36, 'Kab. Lebak');
INSERT INTO `kota_kab` VALUES (3603, 36, 'Kab. Tangerang');
INSERT INTO `kota_kab` VALUES (3604, 36, 'Kab. Serang');
INSERT INTO `kota_kab` VALUES (3671, 36, 'Kota Tangerang');
INSERT INTO `kota_kab` VALUES (3672, 36, 'Kota Cilegon');
INSERT INTO `kota_kab` VALUES (3673, 36, 'Kota Serang');
INSERT INTO `kota_kab` VALUES (3674, 36, 'Kota Tangerang Selatan');
INSERT INTO `kota_kab` VALUES (5101, 51, 'Kab. Jembrana');
INSERT INTO `kota_kab` VALUES (5102, 51, 'Kab. Tabanan');
INSERT INTO `kota_kab` VALUES (5103, 51, 'Kab. Badung');
INSERT INTO `kota_kab` VALUES (5104, 51, 'Kab. Gianyar');
INSERT INTO `kota_kab` VALUES (5105, 51, 'Kab. Klungkung');
INSERT INTO `kota_kab` VALUES (5106, 51, 'Kab. Bangli');
INSERT INTO `kota_kab` VALUES (5107, 51, 'Kab. Karang Asem');
INSERT INTO `kota_kab` VALUES (5108, 51, 'Kab. Buleleng');
INSERT INTO `kota_kab` VALUES (5171, 51, 'Kota Denpasar');
INSERT INTO `kota_kab` VALUES (5201, 52, 'Kab. Lombok Barat');
INSERT INTO `kota_kab` VALUES (5202, 52, 'Kab. Lombok Tengah');
INSERT INTO `kota_kab` VALUES (5203, 52, 'Kab. Lombok Timur');
INSERT INTO `kota_kab` VALUES (5204, 52, 'Kab. Sumbawa');
INSERT INTO `kota_kab` VALUES (5205, 52, 'Kab. Dompu');
INSERT INTO `kota_kab` VALUES (5206, 52, 'Kab. Bima');
INSERT INTO `kota_kab` VALUES (5207, 52, 'Kab. Sumbawa Barat');
INSERT INTO `kota_kab` VALUES (5208, 52, 'Kab. Lombok Utara');
INSERT INTO `kota_kab` VALUES (5271, 52, 'Kota Mataram');
INSERT INTO `kota_kab` VALUES (5272, 52, 'Kota Bima');
INSERT INTO `kota_kab` VALUES (5301, 53, 'Kab. Sumba Barat');
INSERT INTO `kota_kab` VALUES (5302, 53, 'Kab. Sumba Timur');
INSERT INTO `kota_kab` VALUES (5303, 53, 'Kab. Kupang');
INSERT INTO `kota_kab` VALUES (5304, 53, 'Kab. Timor Tengah Selatan');
INSERT INTO `kota_kab` VALUES (5305, 53, 'Kab. Timor Tengah Utara');
INSERT INTO `kota_kab` VALUES (5306, 53, 'Kab. Belu');
INSERT INTO `kota_kab` VALUES (5307, 53, 'Kab. Alor');
INSERT INTO `kota_kab` VALUES (5308, 53, 'Kab. Lembata');
INSERT INTO `kota_kab` VALUES (5309, 53, 'Kab. Flores Timur');
INSERT INTO `kota_kab` VALUES (5310, 53, 'Kab. Sikka');
INSERT INTO `kota_kab` VALUES (5311, 53, 'Kab. Ende');
INSERT INTO `kota_kab` VALUES (5312, 53, 'Kab. Ngada');
INSERT INTO `kota_kab` VALUES (5313, 53, 'Kab. Manggarai');
INSERT INTO `kota_kab` VALUES (5314, 53, 'Kab. Rote Ndao');
INSERT INTO `kota_kab` VALUES (5315, 53, 'Kab. Manggarai Barat');
INSERT INTO `kota_kab` VALUES (5316, 53, 'Kab. Sumba Tengah');
INSERT INTO `kota_kab` VALUES (5317, 53, 'Kab. Sumba Barat Daya');
INSERT INTO `kota_kab` VALUES (5318, 53, 'Kab. Nagekeo');
INSERT INTO `kota_kab` VALUES (5319, 53, 'Kab. Manggarai Timur');
INSERT INTO `kota_kab` VALUES (5320, 53, 'Kab. Sabu Raijua');
INSERT INTO `kota_kab` VALUES (5371, 53, 'Kota Kupang');
INSERT INTO `kota_kab` VALUES (6101, 61, 'Kab. Sambas');
INSERT INTO `kota_kab` VALUES (6102, 61, 'Kab. Bengkayang');
INSERT INTO `kota_kab` VALUES (6103, 61, 'Kab. Landak');
INSERT INTO `kota_kab` VALUES (6104, 61, 'Kab. Pontianak');
INSERT INTO `kota_kab` VALUES (6105, 61, 'Kab. Sanggau');
INSERT INTO `kota_kab` VALUES (6106, 61, 'Kab. Ketapang');
INSERT INTO `kota_kab` VALUES (6107, 61, 'Kab. Sintang');
INSERT INTO `kota_kab` VALUES (6108, 61, 'Kab. Kapuas Hulu');
INSERT INTO `kota_kab` VALUES (6109, 61, 'Kab. Sekadau');
INSERT INTO `kota_kab` VALUES (6110, 61, 'Kab. Melawi');
INSERT INTO `kota_kab` VALUES (6111, 61, 'Kab. Kayong Utara');
INSERT INTO `kota_kab` VALUES (6112, 61, 'Kab. Kubu Raya');
INSERT INTO `kota_kab` VALUES (6171, 61, 'Kota Pontianak');
INSERT INTO `kota_kab` VALUES (6172, 61, 'Kota Singkawang');
INSERT INTO `kota_kab` VALUES (6201, 62, 'Kab. Kotawaringin Barat');
INSERT INTO `kota_kab` VALUES (6202, 62, 'Kab. Kotawaringin Timur');
INSERT INTO `kota_kab` VALUES (6203, 62, 'Kab. Kapuas');
INSERT INTO `kota_kab` VALUES (6204, 62, 'Kab. Barito Selatan');
INSERT INTO `kota_kab` VALUES (6205, 62, 'Kab. Barito Utara');
INSERT INTO `kota_kab` VALUES (6206, 62, 'Kab. Sukamara');
INSERT INTO `kota_kab` VALUES (6207, 62, 'Kab. Lamandau');
INSERT INTO `kota_kab` VALUES (6208, 62, 'Kab. Seruyan');
INSERT INTO `kota_kab` VALUES (6209, 62, 'Kab. Katingan');
INSERT INTO `kota_kab` VALUES (6210, 62, 'Kab. Pulang Pisau');
INSERT INTO `kota_kab` VALUES (6211, 62, 'Kab. Gunung Mas');
INSERT INTO `kota_kab` VALUES (6212, 62, 'Kab. Barito Timur');
INSERT INTO `kota_kab` VALUES (6213, 62, 'Kab. Murung Raya');
INSERT INTO `kota_kab` VALUES (6271, 62, 'Kota Palangka Raya');
INSERT INTO `kota_kab` VALUES (6301, 63, 'Kab. Tanah Laut');
INSERT INTO `kota_kab` VALUES (6302, 63, 'Kab. Kota Baru');
INSERT INTO `kota_kab` VALUES (6303, 63, 'Kab. Banjar');
INSERT INTO `kota_kab` VALUES (6304, 63, 'Kab. Barito Kuala');
INSERT INTO `kota_kab` VALUES (6305, 63, 'Kab. Tapin');
INSERT INTO `kota_kab` VALUES (6306, 63, 'Kab. Hulu Sungai Selatan');
INSERT INTO `kota_kab` VALUES (6307, 63, 'Kab. Hulu Sungai Tengah');
INSERT INTO `kota_kab` VALUES (6308, 63, 'Kab. Hulu Sungai Utara');
INSERT INTO `kota_kab` VALUES (6309, 63, 'Kab. Tabalong');
INSERT INTO `kota_kab` VALUES (6310, 63, 'Kab. Tanah Bumbu');
INSERT INTO `kota_kab` VALUES (6311, 63, 'Kab. Balangan');
INSERT INTO `kota_kab` VALUES (6371, 63, 'Kota Banjarmasin');
INSERT INTO `kota_kab` VALUES (6372, 63, 'Kota Banjar Baru');
INSERT INTO `kota_kab` VALUES (6401, 64, 'Kab. Paser');
INSERT INTO `kota_kab` VALUES (6402, 64, 'Kab. Kutai Barat');
INSERT INTO `kota_kab` VALUES (6403, 64, 'Kab. Kutai Kartanegara');
INSERT INTO `kota_kab` VALUES (6404, 64, 'Kab. Kutai Timur');
INSERT INTO `kota_kab` VALUES (6405, 64, 'Kab. Berau');
INSERT INTO `kota_kab` VALUES (6409, 64, 'Kab. Penajam Paser Utara');
INSERT INTO `kota_kab` VALUES (6471, 64, 'Kota Balikpapan');
INSERT INTO `kota_kab` VALUES (6472, 64, 'Kota Samarinda');
INSERT INTO `kota_kab` VALUES (6474, 64, 'Kota Bontang');
INSERT INTO `kota_kab` VALUES (6501, 65, 'Kab. Malinau');
INSERT INTO `kota_kab` VALUES (6502, 65, 'Kab. Bulungan');
INSERT INTO `kota_kab` VALUES (6503, 65, 'Kab. Tana Tidung');
INSERT INTO `kota_kab` VALUES (6504, 65, 'Kab. Nunukan');
INSERT INTO `kota_kab` VALUES (6571, 65, 'Kota Tarakan');
INSERT INTO `kota_kab` VALUES (7101, 71, 'Kab. Bolaang Mongondow');
INSERT INTO `kota_kab` VALUES (7102, 71, 'Kab. Minahasa');
INSERT INTO `kota_kab` VALUES (7103, 71, 'Kab. Kepulauan Sangihe');
INSERT INTO `kota_kab` VALUES (7104, 71, 'Kab. Kepulauan Talaud');
INSERT INTO `kota_kab` VALUES (7105, 71, 'Kab. Minahasa Selatan');
INSERT INTO `kota_kab` VALUES (7106, 71, 'Kab. Minahasa Utara');
INSERT INTO `kota_kab` VALUES (7107, 71, 'Kab. Bolaang Mongondow Utara');
INSERT INTO `kota_kab` VALUES (7108, 71, 'Kab. Siau Tagulandang Biaro');
INSERT INTO `kota_kab` VALUES (7109, 71, 'Kab. Minahasa Tenggara');
INSERT INTO `kota_kab` VALUES (7110, 71, 'Kab. Bolaang Mongondow Selatan');
INSERT INTO `kota_kab` VALUES (7111, 71, 'Kab. Bolaang Mongondow Timur');
INSERT INTO `kota_kab` VALUES (7171, 71, 'Kota Manado');
INSERT INTO `kota_kab` VALUES (7172, 71, 'Kota Bitung');
INSERT INTO `kota_kab` VALUES (7173, 71, 'Kota Tomohon');
INSERT INTO `kota_kab` VALUES (7174, 71, 'Kota Kotamobagu');
INSERT INTO `kota_kab` VALUES (7201, 72, 'Kab. Banggai Kepulauan');
INSERT INTO `kota_kab` VALUES (7202, 72, 'Kab. Banggai');
INSERT INTO `kota_kab` VALUES (7203, 72, 'Kab. Morowali');
INSERT INTO `kota_kab` VALUES (7204, 72, 'Kab. Poso');
INSERT INTO `kota_kab` VALUES (7205, 72, 'Kab. Donggala');
INSERT INTO `kota_kab` VALUES (7206, 72, 'Kab. Toli-toli');
INSERT INTO `kota_kab` VALUES (7207, 72, 'Kab. Buol');
INSERT INTO `kota_kab` VALUES (7208, 72, 'Kab. Parigi Moutong');
INSERT INTO `kota_kab` VALUES (7209, 72, 'Kab. Tojo Una-una');
INSERT INTO `kota_kab` VALUES (7210, 72, 'Kab. Sigi');
INSERT INTO `kota_kab` VALUES (7271, 72, 'Kota Palu');
INSERT INTO `kota_kab` VALUES (7301, 73, 'Kab. Kepulauan Selayar');
INSERT INTO `kota_kab` VALUES (7302, 73, 'Kab. Bulukumba');
INSERT INTO `kota_kab` VALUES (7303, 73, 'Kab. Bantaeng');
INSERT INTO `kota_kab` VALUES (7304, 73, 'Kab. Jeneponto');
INSERT INTO `kota_kab` VALUES (7305, 73, 'Kab. Takalar');
INSERT INTO `kota_kab` VALUES (7306, 73, 'Kab. Gowa');
INSERT INTO `kota_kab` VALUES (7307, 73, 'Kab. Sinjai');
INSERT INTO `kota_kab` VALUES (7308, 73, 'Kab. Maros');
INSERT INTO `kota_kab` VALUES (7309, 73, 'Kab. Pangkajene Dan Kepulauan');
INSERT INTO `kota_kab` VALUES (7310, 73, 'Kab. Barru');
INSERT INTO `kota_kab` VALUES (7311, 73, 'Kab. Bone');
INSERT INTO `kota_kab` VALUES (7312, 73, 'Kab. Soppeng');
INSERT INTO `kota_kab` VALUES (7313, 73, 'Kab. Wajo');
INSERT INTO `kota_kab` VALUES (7314, 73, 'Kab. Sidenreng Rappang');
INSERT INTO `kota_kab` VALUES (7315, 73, 'Kab. Pinrang');
INSERT INTO `kota_kab` VALUES (7316, 73, 'Kab. Enrekang');
INSERT INTO `kota_kab` VALUES (7317, 73, 'Kab. Luwu');
INSERT INTO `kota_kab` VALUES (7318, 73, 'Kab. Tana Toraja');
INSERT INTO `kota_kab` VALUES (7322, 73, 'Kab. Luwu Utara');
INSERT INTO `kota_kab` VALUES (7325, 73, 'Kab. Luwu Timur');
INSERT INTO `kota_kab` VALUES (7326, 73, 'Kab. Toraja Utara');
INSERT INTO `kota_kab` VALUES (7371, 73, 'Kota Makassar');
INSERT INTO `kota_kab` VALUES (7372, 73, 'Kota Parepare');
INSERT INTO `kota_kab` VALUES (7373, 73, 'Kota Palopo');
INSERT INTO `kota_kab` VALUES (7401, 74, 'Kab. Buton');
INSERT INTO `kota_kab` VALUES (7402, 74, 'Kab. Muna');
INSERT INTO `kota_kab` VALUES (7403, 74, 'Kab. Konawe');
INSERT INTO `kota_kab` VALUES (7404, 74, 'Kab. Kolaka');
INSERT INTO `kota_kab` VALUES (7405, 74, 'Kab. Konawe Selatan');
INSERT INTO `kota_kab` VALUES (7406, 74, 'Kab. Bombana');
INSERT INTO `kota_kab` VALUES (7407, 74, 'Kab. Wakatobi');
INSERT INTO `kota_kab` VALUES (7408, 74, 'Kab. Kolaka Utara');
INSERT INTO `kota_kab` VALUES (7409, 74, 'Kab. Buton Utara');
INSERT INTO `kota_kab` VALUES (7410, 74, 'Kab. Konawe Utara');
INSERT INTO `kota_kab` VALUES (7471, 74, 'Kota Kendari');
INSERT INTO `kota_kab` VALUES (7472, 74, 'Kota Baubau');
INSERT INTO `kota_kab` VALUES (7501, 75, 'Kab. Boalemo');
INSERT INTO `kota_kab` VALUES (7502, 75, 'Kab. Gorontalo');
INSERT INTO `kota_kab` VALUES (7503, 75, 'Kab. Pohuwato');
INSERT INTO `kota_kab` VALUES (7504, 75, 'Kab. Bone Bolango');
INSERT INTO `kota_kab` VALUES (7505, 75, 'Kab. Gorontalo Utara');
INSERT INTO `kota_kab` VALUES (7571, 75, 'Kota Gorontalo');
INSERT INTO `kota_kab` VALUES (7601, 76, 'Kab. Majene');
INSERT INTO `kota_kab` VALUES (7602, 76, 'Kab. Polewali Mandar');
INSERT INTO `kota_kab` VALUES (7603, 76, 'Kab. Mamasa');
INSERT INTO `kota_kab` VALUES (7604, 76, 'Kab. Mamuju');
INSERT INTO `kota_kab` VALUES (7605, 76, 'Kab. Mamuju Utara');
INSERT INTO `kota_kab` VALUES (8101, 81, 'Kab. Maluku Tenggara Barat');
INSERT INTO `kota_kab` VALUES (8102, 81, 'Kab. Maluku Tenggara');
INSERT INTO `kota_kab` VALUES (8103, 81, 'Kab. Maluku Tengah');
INSERT INTO `kota_kab` VALUES (8104, 81, 'Kab. Buru');
INSERT INTO `kota_kab` VALUES (8105, 81, 'Kab. Kepulauan Aru');
INSERT INTO `kota_kab` VALUES (8106, 81, 'Kab. Seram Bagian Barat');
INSERT INTO `kota_kab` VALUES (8107, 81, 'Kab. Seram Bagian Timur');
INSERT INTO `kota_kab` VALUES (8108, 81, 'Kab. Maluku Barat Daya');
INSERT INTO `kota_kab` VALUES (8109, 81, 'Kab. Buru Selatan');
INSERT INTO `kota_kab` VALUES (8171, 81, 'Kota Ambon');
INSERT INTO `kota_kab` VALUES (8172, 81, 'Kota Tual');
INSERT INTO `kota_kab` VALUES (8201, 82, 'Kab. Halmahera Barat');
INSERT INTO `kota_kab` VALUES (8202, 82, 'Kab. Halmahera Tengah');
INSERT INTO `kota_kab` VALUES (8203, 82, 'Kab. Kepulauan Sula');
INSERT INTO `kota_kab` VALUES (8204, 82, 'Kab. Halmahera Selatan');
INSERT INTO `kota_kab` VALUES (8205, 82, 'Kab. Halmahera Utara');
INSERT INTO `kota_kab` VALUES (8206, 82, 'Kab. Halmahera Timur');
INSERT INTO `kota_kab` VALUES (8207, 82, 'Kab. Pulau Morotai');
INSERT INTO `kota_kab` VALUES (8271, 82, 'Kota Ternate');
INSERT INTO `kota_kab` VALUES (8272, 82, 'Kota Tidore Kepulauan');
INSERT INTO `kota_kab` VALUES (9101, 91, 'Kab. Fakfak');
INSERT INTO `kota_kab` VALUES (9102, 91, 'Kab. Kaimana');
INSERT INTO `kota_kab` VALUES (9103, 91, 'Kab. Teluk Wondama');
INSERT INTO `kota_kab` VALUES (9104, 91, 'Kab. Teluk Bintuni');
INSERT INTO `kota_kab` VALUES (9105, 91, 'Kab. Manokwari');
INSERT INTO `kota_kab` VALUES (9106, 91, 'Kab. Sorong Selatan');
INSERT INTO `kota_kab` VALUES (9107, 91, 'Kab. Sorong');
INSERT INTO `kota_kab` VALUES (9108, 91, 'Kab. Raja Ampat');
INSERT INTO `kota_kab` VALUES (9109, 91, 'Kab. Tambrauw');
INSERT INTO `kota_kab` VALUES (9110, 91, 'Kab. Maybrat');
INSERT INTO `kota_kab` VALUES (9171, 91, 'Kota Sorong');
INSERT INTO `kota_kab` VALUES (9401, 94, 'Kab. Merauke');
INSERT INTO `kota_kab` VALUES (9402, 94, 'Kab. Jayawijaya');
INSERT INTO `kota_kab` VALUES (9403, 94, 'Kab. Jayapura');
INSERT INTO `kota_kab` VALUES (9404, 94, 'Kab. Nabire');
INSERT INTO `kota_kab` VALUES (9408, 94, 'Kab. Kepulauan Yapen');
INSERT INTO `kota_kab` VALUES (9409, 94, 'Kab. Biak Numfor');
INSERT INTO `kota_kab` VALUES (9410, 94, 'Kab. Paniai');
INSERT INTO `kota_kab` VALUES (9411, 94, 'Kab. Puncak Jaya');
INSERT INTO `kota_kab` VALUES (9412, 94, 'Kab. Mimika');
INSERT INTO `kota_kab` VALUES (9413, 94, 'Kab. Boven Digoel');
INSERT INTO `kota_kab` VALUES (9414, 94, 'Kab. Mappi');
INSERT INTO `kota_kab` VALUES (9415, 94, 'Kab. Asmat');
INSERT INTO `kota_kab` VALUES (9416, 94, 'Kab. Yahukimo');
INSERT INTO `kota_kab` VALUES (9417, 94, 'Kab. Pegunungan Bintang');
INSERT INTO `kota_kab` VALUES (9418, 94, 'Kab. Tolikara');
INSERT INTO `kota_kab` VALUES (9419, 94, 'Kab. Sarmi');
INSERT INTO `kota_kab` VALUES (9420, 94, 'Kab. Keerom');
INSERT INTO `kota_kab` VALUES (9426, 94, 'Kab. Waropen');
INSERT INTO `kota_kab` VALUES (9427, 94, 'Kab. Supiori');
INSERT INTO `kota_kab` VALUES (9428, 94, 'Kab. Mamberamo Raya');
INSERT INTO `kota_kab` VALUES (9429, 94, 'Kab. Nduga');
INSERT INTO `kota_kab` VALUES (9430, 94, 'Kab. Lanny Jaya');
INSERT INTO `kota_kab` VALUES (9431, 94, 'Kab. Mamberamo Tengah');
INSERT INTO `kota_kab` VALUES (9432, 94, 'Kab. Yalimo');
INSERT INTO `kota_kab` VALUES (9433, 94, 'Kab. Puncak');
INSERT INTO `kota_kab` VALUES (9434, 94, 'Kab. Dogiyai');
INSERT INTO `kota_kab` VALUES (9435, 94, 'Kab. Intan Jaya');
INSERT INTO `kota_kab` VALUES (9436, 94, 'Kab. Deiyai');
INSERT INTO `kota_kab` VALUES (9471, 94, 'Kota Jayapura');

-- ----------------------------
-- Table structure for nilai
-- ----------------------------
DROP TABLE IF EXISTS `nilai`;
CREATE TABLE `nilai`  (
  `id_nilai` int(11) NOT NULL AUTO_INCREMENT,
  `id_jadwal` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `kode_indikator` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tanggal_nilai` date NOT NULL,
  `nilai_perkembangan` int(11) NOT NULL,
  `deskripsi` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_teknik` int(11) NOT NULL,
  `foto` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_nilai`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for perencanaan
-- ----------------------------
DROP TABLE IF EXISTS `perencanaan`;
CREATE TABLE `perencanaan`  (
  `id_perencanaan` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal_awal` date NOT NULL,
  `tanggal_akhir` date NOT NULL,
  `id_tahunajaran` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL,
  `id_kelas` varchar(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_program` int(11) NOT NULL,
  `id_tema` int(11) NOT NULL,
  `id_subtema` int(11) NOT NULL,
  `id_subsubtema` int(11) NOT NULL,
  `id_kd` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_indikator` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `materi` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_perencanaan`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for program
-- ----------------------------
DROP TABLE IF EXISTS `program`;
CREATE TABLE `program`  (
  `id_program` int(11) NOT NULL AUTO_INCREMENT,
  `nama_program` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_program`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of program
-- ----------------------------
INSERT INTO `program` VALUES (1, '(NAM) Nilai Agama dan Moral ');
INSERT INTO `program` VALUES (2, '(FM) Fisik Motorik');
INSERT INTO `program` VALUES (3, '(KOG) Kognitif');
INSERT INTO `program` VALUES (4, '(Bahasa)BAHASA');
INSERT INTO `program` VALUES (5, 'Sosial Emosional');
INSERT INTO `program` VALUES (6, 'Seni');

-- ----------------------------
-- Table structure for provinsi
-- ----------------------------
DROP TABLE IF EXISTS `provinsi`;
CREATE TABLE `provinsi`  (
  `id_provinsi` int(11) NOT NULL,
  `nama_provinsi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_provinsi`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of provinsi
-- ----------------------------
INSERT INTO `provinsi` VALUES (11, 'Aceh');
INSERT INTO `provinsi` VALUES (12, 'Sumatera Utara');
INSERT INTO `provinsi` VALUES (13, 'Sumatera Barat');
INSERT INTO `provinsi` VALUES (14, 'Riau');
INSERT INTO `provinsi` VALUES (15, 'Jambi');
INSERT INTO `provinsi` VALUES (16, 'Sumatera Selatan');
INSERT INTO `provinsi` VALUES (17, 'Bengkulu');
INSERT INTO `provinsi` VALUES (18, 'Lampung');
INSERT INTO `provinsi` VALUES (19, 'Kepulauan Bangka Belitung');
INSERT INTO `provinsi` VALUES (21, 'Kepulauan Riau');
INSERT INTO `provinsi` VALUES (31, 'Dki Jakarta');
INSERT INTO `provinsi` VALUES (32, 'Jawa Barat');
INSERT INTO `provinsi` VALUES (33, 'Jawa Tengah');
INSERT INTO `provinsi` VALUES (34, 'Di Yogyakarta');
INSERT INTO `provinsi` VALUES (35, 'Jawa Timur');
INSERT INTO `provinsi` VALUES (36, 'Banten');
INSERT INTO `provinsi` VALUES (51, 'Bali');
INSERT INTO `provinsi` VALUES (52, 'Nusa Tenggara Barat');
INSERT INTO `provinsi` VALUES (53, 'Nusa Tenggara Timur');
INSERT INTO `provinsi` VALUES (61, 'Kalimantan Barat');
INSERT INTO `provinsi` VALUES (62, 'Kalimantan Tengah');
INSERT INTO `provinsi` VALUES (63, 'Kalimantan Selatan');
INSERT INTO `provinsi` VALUES (64, 'Kalimantan Timur');
INSERT INTO `provinsi` VALUES (65, 'Kalimantan Utara');
INSERT INTO `provinsi` VALUES (71, 'Sulawesi Utara');
INSERT INTO `provinsi` VALUES (72, 'Sulawesi Tengah');
INSERT INTO `provinsi` VALUES (73, 'Sulawesi Selatan');
INSERT INTO `provinsi` VALUES (74, 'Sulawesi Tenggara');
INSERT INTO `provinsi` VALUES (75, 'Gorontalo');
INSERT INTO `provinsi` VALUES (76, 'Sulawesi Barat');
INSERT INTO `provinsi` VALUES (81, 'Maluku');
INSERT INTO `provinsi` VALUES (82, 'Maluku Utara');
INSERT INTO `provinsi` VALUES (91, 'Papua Barat');
INSERT INTO `provinsi` VALUES (94, 'Papua');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `id_role` int(11) NOT NULL AUTO_INCREMENT,
  `nama_role` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_role`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES (1, 'guru');
INSERT INTO `role` VALUES (2, 'pusat');
INSERT INTO `role` VALUES (3, 'sekolah');
INSERT INTO `role` VALUES (4, 'kanwil');

-- ----------------------------
-- Table structure for sekolah
-- ----------------------------
DROP TABLE IF EXISTS `sekolah`;
CREATE TABLE `sekolah`  (
  `id_sekolah` int(11) NOT NULL AUTO_INCREMENT,
  `nama_sekolah` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `NSN` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jalan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `kelurahan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `kecamatan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `kode_pos` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `id_kotaKab` int(11) NULL DEFAULT NULL,
  `id_provinsi` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `website` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `telepon` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `email` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `kepala_RA` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `tahun_ajaran` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `foto` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `status` int(11) NOT NULL,
  `tgl_daftar` datetime(0) NULL DEFAULT NULL,
  `verifikator` int(11) NULL DEFAULT NULL,
  `tgl_verif` datetime(0) NULL DEFAULT NULL,
  `id_user_delete` int(11) NULL DEFAULT NULL,
  `tanggal_delete` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_sekolah`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sekolah_delete
-- ----------------------------
DROP TABLE IF EXISTS `sekolah_delete`;
CREATE TABLE `sekolah_delete`  (
  `id_sekolah` int(11) NOT NULL AUTO_INCREMENT,
  `nama_sekolah` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `NSN` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jalan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `kelurahan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `kecamatan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `kode_pos` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `id_kotaKab` int(11) NULL DEFAULT NULL,
  `id_provinsi` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `website` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `telepon` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `email` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `kepala_RA` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `tahun_ajaran` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `foto` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `status` int(11) NOT NULL,
  `tgl_daftar` datetime(0) NULL DEFAULT NULL,
  `verifikator` int(11) NOT NULL,
  `tgl_verif` datetime(0) NOT NULL,
  `id_user_delete` int(11) NULL DEFAULT NULL,
  `tgl_delete` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_sekolah`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for semester
-- ----------------------------
DROP TABLE IF EXISTS `semester`;
CREATE TABLE `semester`  (
  `id_semester` int(11) NOT NULL AUTO_INCREMENT,
  `nama_semester` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_semester`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of semester
-- ----------------------------
INSERT INTO `semester` VALUES (1, 'Semester 1');
INSERT INTO `semester` VALUES (2, 'Semester 2');

-- ----------------------------
-- Table structure for siswa
-- ----------------------------
DROP TABLE IF EXISTS `siswa`;
CREATE TABLE `siswa`  (
  `id_siswa` int(11) NOT NULL AUTO_INCREMENT,
  `id_sekolah` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `nama_siswa` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama_panggilan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `no_induk` int(11) NOT NULL,
  `tempat_lahir` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `tanggal_lahir` date NULL DEFAULT NULL,
  `jenis_kelamin` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `agama` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `nama_ayah` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `nama_ibu` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `pekerjaan_ayah` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `pekerjaan_ibu` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `status_anak` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `anak_ke` int(11) NULL DEFAULT NULL,
  `jalan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `kelurahan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `kecamatan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `provinsi` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `kotakab` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `foto` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `diterima_tanggal` date NULL DEFAULT NULL,
  `id_tahunajaran` int(11) NOT NULL,
  `nama_wali` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `pekerjaan_wali` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `alamat_wali` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `telepon_wali` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  PRIMARY KEY (`id_siswa`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for subsubtema
-- ----------------------------
DROP TABLE IF EXISTS `subsubtema`;
CREATE TABLE `subsubtema`  (
  `id_subsubtema` int(11) NOT NULL AUTO_INCREMENT,
  `id_tema` int(11) NOT NULL,
  `id_subtema` int(11) NOT NULL,
  `nama` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_subsubtema`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for subtema
-- ----------------------------
DROP TABLE IF EXISTS `subtema`;
CREATE TABLE `subtema`  (
  `id_subtema` int(11) NOT NULL,
  `id_tema` int(11) NOT NULL,
  `nama_subtema` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_kd` int(11) NOT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for tahun_ajaran
-- ----------------------------
DROP TABLE IF EXISTS `tahun_ajaran`;
CREATE TABLE `tahun_ajaran`  (
  `id_tahunajaran` int(11) NOT NULL AUTO_INCREMENT,
  `nama_tahun` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_tahunajaran`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tahun_ajaran
-- ----------------------------
INSERT INTO `tahun_ajaran` VALUES (1, '2016/2017');
INSERT INTO `tahun_ajaran` VALUES (2, '2017/2018');
INSERT INTO `tahun_ajaran` VALUES (3, '2018/2019');
INSERT INTO `tahun_ajaran` VALUES (4, '2019/2020');
INSERT INTO `tahun_ajaran` VALUES (5, '2020/2021');
INSERT INTO `tahun_ajaran` VALUES (6, '2021/2022');

-- ----------------------------
-- Table structure for teknik_penugasan
-- ----------------------------
DROP TABLE IF EXISTS `teknik_penugasan`;
CREATE TABLE `teknik_penugasan`  (
  `id_teknik` int(11) NOT NULL AUTO_INCREMENT,
  `nama_teknik` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_teknik`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of teknik_penugasan
-- ----------------------------
INSERT INTO `teknik_penugasan` VALUES (1, 'Observasi');
INSERT INTO `teknik_penugasan` VALUES (2, 'Penugasan');
INSERT INTO `teknik_penugasan` VALUES (3, 'Unjuk kerja');
INSERT INTO `teknik_penugasan` VALUES (4, 'Anekdot');
INSERT INTO `teknik_penugasan` VALUES (5, 'Hasil Karya');

-- ----------------------------
-- Table structure for tingkat
-- ----------------------------
DROP TABLE IF EXISTS `tingkat`;
CREATE TABLE `tingkat`  (
  `id_tingkat` int(11) NOT NULL AUTO_INCREMENT,
  `nama_tingkat` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_tingkat`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tingkat
-- ----------------------------
INSERT INTO `tingkat` VALUES (1, 'Kelompok A');
INSERT INTO `tingkat` VALUES (2, 'Kelompok B');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `id_role` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `id_guru` int(11) NOT NULL,
  `id_provinsi` int(11) NOT NULL,
  PRIMARY KEY (`id_user`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'admin', 'bismi11ah', -1, 2, 1, 0, 0);

-- ----------------------------
-- Table structure for user__delete
-- ----------------------------
DROP TABLE IF EXISTS `user__delete`;
CREATE TABLE `user__delete`  (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `id_role` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `id_guru` int(11) NOT NULL,
  `id_provinsi` int(11) NOT NULL,
  PRIMARY KEY (`id_user`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for user__download
-- ----------------------------
DROP TABLE IF EXISTS `user__download`;
CREATE TABLE `user__download`  (
  `id_download` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `tanggal` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `id_sekolah` int(11) NOT NULL,
  PRIMARY KEY (`id_download`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for user__logs
-- ----------------------------
DROP TABLE IF EXISTS `user__logs`;
CREATE TABLE `user__logs`  (
  `id_log` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `browser_log` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `platform_log` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ip_log` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `date_login` datetime(0) NOT NULL,
  `date_logout` datetime(0) NOT NULL,
  `browser_version` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_log`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for user__logs_regis
-- ----------------------------
DROP TABLE IF EXISTS `user__logs_regis`;
CREATE TABLE `user__logs_regis`  (
  `id_log` int(11) NOT NULL AUTO_INCREMENT,
  `id_sekolah` int(11) NOT NULL,
  `browser_log` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `platform_log` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ip_log` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `date_login` datetime(0) NOT NULL,
  `browser_version` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_log`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
